/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.madresistor.box0_studio.extra;

import android.support.annotation.ArrayRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.res.TypedArray;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A concrete BaseAdapter that is backed by an array of arbitrary
 * objects of image and text.  By default this class expects that the provided resource id references
 * a single TextView and ImageView.  If you want to use a more complex layout, use the constructors that
 * also takes a field id.  That field id's should reference a TextView and ImageView in the larger layout
 * resource.
 *
 * <p>However the TextView is referenced, it will be filled with the toString() of each object in
 * the array. You can add lists or arrays of custom objects. Override the toString() method
 * of your objects to determine what text will be displayed for the item in the list.
 *
 * <p>To use something other than TextViews for the array display, for instance, another ImageView,
 * or to have some of data besides toString() results fill the views,
 * override {@link #getView(int, View, ViewGroup)} to return the type of view you want.
 */
public class ImageWithTextAdapter<T> extends BaseAdapter {
    private final static String TAG = ImageWithTextAdapter.class.getName();
    private final LayoutInflater mInflater;

    /**
     * Contains the list of objects that represent the data of this ImageWithTextAdapter.
     * The content of this list is referred to as "the array" in the documentation.
     */
    private T[] mTextObjects;
    private int[] mImageObjects;

    /**
     * The resource indicating what views to inflate to display the content of this
     * array adapter.
     */
    private int mResource;

    /**
     * The resource indicating what views to inflate to display the content of this
     * array adapter in a drop down widget.
     */
    private int mDropDownResource;

    /**
     * If the inflated resource is not a TextView, {@link #mFieldId} is used to find
     * a TextView inside the inflated views hierarchy. This field must contain the
     * identifier that matches the one defined in the resource file.
     */
    private int mTextFieldId;
    private int mImageFieldId;

    private Context mContext;

    /**
     * Constructor
     *
     * @param context The current context.
     * @param layoutResId The resource ID for a layout file containing a layout to use when
     *                 instantiating views.
     * @param imgViewResId The id of the ImageView within the layout resource to be populated
     * @param textViewResId The id of the TextView within the layout resource to be populated
     * @param imgageObjects The objects to represent in the ListView ImageView.
     * @param textObjects The objects to represent in the ListView TextView.
     */
    public ImageWithTextAdapter(Context context,
            int[] imageObjects, T[] textObjects,
            @IdRes int imageViewResId, @IdRes int textViewResId,
            @LayoutRes int layoutResId) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mImageObjects = imageObjects;
        mTextObjects = textObjects;
        mImageFieldId = imageViewResId;
        mTextFieldId = textViewResId;
        mResource = mDropDownResource = layoutResId;
    }

    /**
     * Returns the context associated with this array adapter. The context is used
     * to create views from the resource passed to the constructor.
     *
     * @return The Context associated with this adapter.
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * {@inheritDoc}
     */
    public int getCount() {
        return Math.min(mTextObjects.length, mImageObjects.length);
    }

    public class Item {
        public final int image;
        public final T text;
        public Item(int i, T t) {
            image = i;
            text = t;
        }
    }

    /**
     * {@inheritDoc}
     */
    public Item getItem(int position) {
        return new Item(mImageObjects[position], mTextObjects[position]);
    }

    /**
     * Returns the position of the specified item in the array.
     *
     * @param item The item to retrieve the position of.
     *
     * @return The position of the specified item.
     */
    public int getPosition(Item item) {
        int count = Math.min(mTextObjects.length, mImageObjects.length);
        for (int i = 0; i < count; i++) {
            if (item.text == mTextObjects[i] && item.image == mImageObjects[i]) {
                return i;
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * {@inheritDoc}
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(mInflater, position, convertView, parent, mResource);
    }

    private View createViewFromResource(LayoutInflater inflater, int position, View convertView,
            ViewGroup parent, int resource) {
        View view;
        TextView text;
        ImageView image;

        if (convertView == null) {
            view = inflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }

        try {
            text = (TextView) view.findViewById(mTextFieldId);
        } catch (ClassCastException e) {
            Log.e(TAG, "You must supply a resource ID for a TextView");
            throw new IllegalStateException(
                    "ImageWithTextAdapter requires the resources ID to be a TextView", e);
        }

        try {
            image = (ImageView) view.findViewById(mImageFieldId);
        } catch (ClassCastException e) {
            Log.e(TAG, "You must supply a resource ID for a ImageView");
            throw new IllegalStateException(
                    "ImageWithTextAdapter requires the resources ID to be a ImageView", e);
        }

        Item item = getItem(position);
        if (item.text instanceof CharSequence) {
            text.setText((CharSequence)item.text);
        } else {
            text.setText(item.text.toString());
        }

        image.setImageResource(item.image);

        return view;
    }

    /**
     * <p>Sets the layout resource to create the drop down views.</p>
     *
     * @param resource the layout resource defining the drop down views
     * @see #getDropDownView(int, android.view.View, android.view.ViewGroup)
     */
    public void setDropDownViewResource(@LayoutRes int resource) {
        this.mDropDownResource = resource;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(mInflater, position, convertView, parent, mDropDownResource);
    }

    /**
     * Convert a typed array to resID int
     * @param arr Typed Array
     * @param def
     * @return Integer array
     */
    private static int[] typedArrayResIds(TypedArray arr, int def) {
        int COUNT = arr.length();
        int[] res = new int[COUNT];
        for (int i = 0; i < COUNT; i++) {
            res[i] = arr.getResourceId(i, def);
        }
        return res;
    }

    /**
     * Creates a new ImageWithTextAdapter from external resources. The content of the array is
     * obtained through {@link android.content.res.Resources#getTextArray(int)} and {@link android.content.res.Resources#obtainTypedArray()} and {@link android.content.res.TypedArray#getResourceId(int, int)}.
     *
     * @param context The application's environment.
     * @param imgArrayResIdThe id of the TextView within the layout resource to be populated
     * @param textArrayResIdThe id of the ImageView within the layout resource to be populated
     * @param viewImgResId The identifier of the widget to extract ImageView.
     * @param viewTextResId The identifier of the widget to extract TextView.
     * @param viewResId The identifier of the layout used to create views.
     *
     * @return An ImageWithTextAdapter<CharSequence>.
     */
    public static ImageWithTextAdapter<CharSequence> createFromResource(Context context,
            @ArrayRes int imgArrayResId, @ArrayRes int textArrayResId,
            @IdRes int imgViewResId, @IdRes int textViewResId, @LayoutRes int viewResId) {
        Resources res = context.getResources();
        CharSequence[] strings = res.getTextArray(textArrayResId);
        TypedArray arr = res.obtainTypedArray(imgArrayResId);
        int[] images = typedArrayResIds(arr, -1);
        arr.recycle();
        return new ImageWithTextAdapter<CharSequence>(context, images, strings,
            imgViewResId, textViewResId, viewResId);
    }
}
