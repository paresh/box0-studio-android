/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.extra;

import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;

import org.madresistor.box0_studio.R;

public class SettingsFragment extends PreferenceFragment {
	public static final String PREF_OSC_PLOT_STYLE = "PREF_OSC_PLOT_STYLE";
	public static final String PREF_LINE_WIDTH = "PREF_LINE_WIDTH";

	public static final boolean DEF_OSC_PLOT_STYLE = false;
	public static final int DEF_LINE_WIDTH = 3;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);
	}
}
