/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.extra.input_filter;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Validator Input filter.
 * This call does all the building of final separate string and
 *  pass on to subclass to verify if the string is valid.
 */
public abstract class ValidatorInputFilter implements InputFilter {

	private static final String EMPTY = "";

	/*
	 * ref: http://stackoverflow.com/a/14212734/1500988
	 * ref: http://stackoverflow.com/a/19072151/1500988
	 */
	@Override
	public CharSequence filter(CharSequence src, int start, int end,
			Spanned dest, int dstart, int dend) {
		String _dest = dest.toString();
		String _src = src.toString();
		String newVal =
			_dest.substring(0, dstart) + /* before */
			_src.substring(start, end) + /* new */
			_dest.substring(dend, _dest.length()); /* after */

		return isValid(newVal) ? null : EMPTY;
	}

	/**
	 * Check if the string is valid
	 * <p>sub-class need to ensure that empty string are accepted</p>
	 * @param value Value
	 * @return true if valid, false on invalid
	 */
	protected abstract boolean isValid(String value);
}
