/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.extra.input_filter;

/**
 * Percentage value input filter
 * Check if the value is a valid percentage value
 * Percentage value can always be between 0, 100 (0, 100 included)
 */
public class PercentInputFilter extends AbsFloatingInputFilter
{
	protected boolean isValid(double val) {
		return (val >= 0f && val <= 100f);
	}
}
