/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2015-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.extra.input_filter;

/**
 * Abstract class for converting string to floating value and
 *  pass on to sub-class to check if the value is valid.
 *
 * If unable to parse the string to floating, value is treated as invalid.
 */
public abstract class AbsFloatingInputFilter extends ValidatorInputFilter
{
	protected boolean isValid(String value) {
		if (value.length() == 0) {
			return true;
		}

		try {
			return isValid(Double.parseDouble(value));
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	/**
	 * Check if the floating value is valid.
	 * @param value Value
	 * @return true if valid, false on invalid
	 */
	abstract protected boolean isValid(double value);
}
