/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio;

import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.view.inputmethod.InputMethodManager;
import android.content.Context;

import org.madresistor.box0_studio.Config;
import org.madresistor.box0_studio.extra.AboutFragment;
import org.madresistor.box0_studio.extra.SettingsFragment;
import org.madresistor.box0_studio.nav_drawer.NavDrawerExtra;
import org.madresistor.box0_studio.nav_drawer.NavDrawerListAdapter;
import org.madresistor.box0_studio.nav_drawer.NavDrawerModule;

import java.util.ArrayList;

abstract class AbsNavDrawerMainActivity extends AbsModuleMainActivity
		implements FragmentManager.OnBackStackChangedListener {
	private final static String TAG = AbsNavDrawerMainActivity.class.getName();
	private ActionBarDrawerToggle m_drawerToggle = null;
	private DrawerLayout m_drawerLayout;
	private ListView m_navDrawerEntries;
	private ArrayList<NavDrawerListAdapter.Entry> m_navDrawerEntriesList;

	private static final int MODULE = 0;
	private static final int MODULE_OSC = MODULE + 0;
	private static final int MODULE_FUNC_GEN = MODULE + 1;
	private static final int MODULE_PWM_PANEL = MODULE + 2;
	private static final int MODULE_DIO_PANEL = MODULE + 3;
	private static final int EXTRA = 4;
	private static final int EXTRA_SETTINGS = EXTRA + 0;
	private static final int EXTRA_ABOUT = EXTRA + 1;

	/* TAG used for saving and load current module from bundle */
	private static final String CURRENT_INDEX = TAG + ".CURRENT_INDEX";
	private static final int DEFAULT_INDEX = MODULE_OSC;

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		/* Sync the toggle state after
		 * onRestoreInstanceState has occurred.
		 */
		m_drawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/* toggle nav drawer on selecting action bar app icon/title */
		return m_drawerToggle.onOptionsItemSelected(item) ||
				super.onOptionsItemSelected(item);
	}

	/**
	 * Hide the soft keyboard (if currently being shown)
	 */
	private void hideSoftKeyboard() {
		InputMethodManager inputMethodManager =
			(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	}

	/** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/* ============ Initalize drawer ====================== */
		m_drawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer_layout);
		m_navDrawerEntries = (ListView) findViewById(R.id.nav_drawer_entries);

		// enabling action bar app icon and behaving it as toggle button
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		m_drawerToggle = new ActionBarDrawerToggle(this, m_drawerLayout,
				//R.drawable.ic_drawer, //nav menu toggle icon
				R.string.nav_drawer_open, // nav drawer open - description for accessibility
				R.string.nav_drawer_close // nav drawer close - description for accessibility
		) {
			public void onDrawerClosed(View view) {
				updateDrawerAndTitle();

				/* calling onPrepareOptionsMenu() to show action bar icons */
				supportInvalidateOptionsMenu();

				hideSoftKeyboard();
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(R.string.app_name);

				/* calling onPrepareOptionsMenu() to hide action bar icons */
				supportInvalidateOptionsMenu();

				hideSoftKeyboard();
			}
		};

		m_drawerLayout.setDrawerListener(m_drawerToggle);

		/* ==================== Insert entries to drawer ============= */
		/* load slide menu items */
		String[] titles = getResources().getStringArray(R.array.nav_drawer_entries_title);
		TypedArray icons = getResources().obtainTypedArray(R.array.nav_drawer_entries_icon);

		m_navDrawerEntriesList = new ArrayList<NavDrawerListAdapter.Entry>();

		for (int i = 0; i < titles.length; i++) {
			String title = titles[i];
			int icon = icons.getResourceId(i, -1);

			NavDrawerListAdapter.Entry entry;
			if (i < EXTRA) {
				entry = new NavDrawerModule(title, icon);
			} else {
				entry = new NavDrawerExtra(title, icon);
			}

			m_navDrawerEntriesList.add(entry);
		}

		/* Recycle the typed array */
		icons.recycle();

		/* setting the nav drawer list adapter */
		NavDrawerListAdapter adapter = new NavDrawerListAdapter(this, m_navDrawerEntriesList);
		m_navDrawerEntries.setAdapter(adapter);
		m_navDrawerEntries.setOnItemClickListener(new DrawerItemClickListener());

		/* initalize fragment manager */
		getSupportFragmentManager().addOnBackStackChangedListener(this);

		/* ==== Load inital fragment ==== */
		int index = DEFAULT_INDEX;
		if (savedInstanceState != null) {
			index = savedInstanceState.getInt(CURRENT_INDEX, DEFAULT_INDEX);
		}
		setCurrentEntry(index, true);
	}

	/* select which item is selected */
	private void updateDrawerAndTitle() {
		int index = getCurrentFragmentIndex();

		m_navDrawerEntries.setItemChecked(index, true);
		m_navDrawerEntries.setSelection(index);

		String title = m_navDrawerEntriesList.get(index).getTitle();
		getSupportActionBar().setTitle(title);
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			setCurrentEntry(position);

			/* Close drawer */
			m_drawerLayout.closeDrawer(m_navDrawerEntries);
		}
	}

	/* when back is pressed, fragment from backstack is loaded
	 *  so we need to update the drawer */
	public void onBackStackChanged() {
		updateDrawerAndTitle();
	}

	private void setCurrentEntry(int index) {
		setCurrentEntry(index, false);
	}

	/* Diplaying fragment view for selected nav drawer list item */
	private void setCurrentEntry(int index, boolean firstTime) {
		if (!(index < m_navDrawerEntriesList.size())) {
			if (Config.ERROR) Log.e(TAG, "Error in creating fragment");
			return;
		}

		/* Fragment Update */
		Fragment fragment = getFragment(index);
		if (fragment == null) {
			if (Config.ERROR) Log.e(TAG, "fragment null, skipping");
			return;
		}

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

		if (firstTime) {
			ft.add(R.id.main_activity_module, fragment);
		} else {
			ft.replace(R.id.main_activity_module, fragment);
			ft.addToBackStack(null);
		}

		ft.commit();

		updateDrawerAndTitle();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		/* stateloss so that if app is not visible, dont cause problem */
		Fragment fragment = getCurrentFragment();
		if (fragment != null) {
			getSupportFragmentManager()
				.beginTransaction()
				.detach(fragment)
				.attach(fragment)
				.commitAllowingStateLoss();
		}

		/* Pass any configuration change to the drawer toggle */
		m_drawerToggle.onConfigurationChanged(newConfig);

		updateDrawerAndTitle();
	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putInt(CURRENT_INDEX, getCurrentFragmentIndex());
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		/* restore current index */
		int index = savedInstanceState.getInt(CURRENT_INDEX, DEFAULT_INDEX);

		/* update the current module on each resume (without commit).
		 * WHY?:state loss exception occur when user rotate screen when app is not visible
		 */
		Fragment fragment = getFragment(index);
		if (fragment != null) {
			getSupportFragmentManager()
				.beginTransaction()
				.detach(fragment)
				.attach(fragment)
				.commit();
		}

		/* update the drawer */
		updateDrawerAndTitle();
	}

	private Fragment getCurrentFragment() {
		return getSupportFragmentManager().
			findFragmentById(R.id.main_activity_module);
	}

	private int getCurrentFragmentIndex() {
		Fragment f = getCurrentFragment();
		if (f == null) {
			if (Config.WTF) Log.wtf(TAG, "Fragment returned by getCurrentFragment() is null");
			// do not crash app (not a big problem), instead use default value
			return DEFAULT_INDEX;
		}

		return getIndex(f);
	}

	private Fragment getFragment(int index) {
		switch(index) {
		case MODULE_OSC:			return setIndex(getModuleFragment(OSC), MODULE_OSC);
		case MODULE_FUNC_GEN:		return setIndex(getModuleFragment(FUNC_GEN), MODULE_FUNC_GEN);
		case MODULE_PWM_PANEL:		return setIndex(getModuleFragment(PWM_PANEL), MODULE_PWM_PANEL);
		case MODULE_DIO_PANEL:		return setIndex(getModuleFragment(DIO_PANEL), MODULE_DIO_PANEL);
		case EXTRA_SETTINGS:		return setIndex(new SettingsFragment(), EXTRA_SETTINGS);
		case EXTRA_ABOUT:			return setIndex(new AboutFragment(), EXTRA_ABOUT);
		default:					return null;
		}
	}

	private Fragment setIndex(Fragment f, int index) {
		Bundle b = f.getArguments();
		if (b == null) {
			b = new Bundle();
			b.putInt("index", index);
			f.setArguments(b);
		}

		return f;
	}

	private int getIndex(Fragment f) {
		Bundle b = f.getArguments();

		if (b == null) {
			if (Config.WTF) Log.wtf(TAG, "Fragment " + f + " getArguments() returned null");
			// do not crash app (not a big problem), instead use default value
			return DEFAULT_INDEX;
		}

		return b.getInt("index", DEFAULT_INDEX);
	}
}
