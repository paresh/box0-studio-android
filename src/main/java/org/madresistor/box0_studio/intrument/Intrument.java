/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.intrument;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;

import org.madresistor.box0.Device;
import org.madresistor.box0_studio.JniLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Instrument are the building block of the Application.
 * An instrument is a block of function that interact with one or module
 *  and provide an user interface for doing something useful with it.
 */
public abstract class Intrument {
	private final Context m_context;

	/**
	 * Constructor
	 * @param context Context
	 */
	public Intrument(Context context) {
		m_context = context;
	}

	/**
	 * Return the context that was passed to constructor
	 * @return context
	 */
	public Context getContext() {
		return m_context;
	}

	/**
	 * Called when a device got connected or disconnected
	 * @param device null if device disconnected, non-null if connected
	 */
	abstract public void setDevice(Device device);

	/**
	 * Free all resource allocated by the instrument
	 * Including device
	 */
	abstract public void freeResource();

	/* get the fragment when user select the module */
	abstract public Frontend getFrontend();

	/* module will call this method when
	 * their is some status(like running) to update
	 */
	abstract public void setOnStatusListener(OnStatusListener onStatusListener);

	public abstract interface OnStatusListener {
		public void onStatusChanged(String text);
		public void setStatusEnabled(boolean enabled);
	}

	/**
	 * Frontend for the module.
	 * All modules implement the frontend for the user to interact
	 */
	public abstract class Frontend extends Fragment {

	}

	/**
	 * Generate a random color for user
	 * @return color
	 */
	public static int newColorForUser() {
		Random randGen = new Random();
		/*
		 * values[0] : Hue (0 .. 360)
		 * values[1] : Saturation (0...1)
		 * values[2] : Value (0...1)
		 */
		if (predefinedColors.isEmpty()) {
			float hue = randGen.nextInt(360001) / 1000.0f;
			float[] values = new float[] { hue, 0.8f, 0.5f };
			return Color.HSVToColor(values);
		} else {
			return predefinedColors.remove(0);
		}
	}

	private static final List<Integer> predefinedColors = new ArrayList<Integer>();

	static {
		JniLoader.glue();

		predefinedColors.add(0xFF800000);
		predefinedColors.add(0xFF008080);
		predefinedColors.add(0xFF0000ff);
		predefinedColors.add(0xFF660066);
		predefinedColors.add(0xFF333333);
		predefinedColors.add(0xFFff7373);
		predefinedColors.add(0xFF088da5);
		predefinedColors.add(0xFF008000);
		predefinedColors.add(0xFF468499);
		predefinedColors.add(0xFFcc0000);
		predefinedColors.add(0xFF800080);
		predefinedColors.add(0xFF191970);
		predefinedColors.add(0xFF006400);
		predefinedColors.add(0xFFff8d00);
	}
}
