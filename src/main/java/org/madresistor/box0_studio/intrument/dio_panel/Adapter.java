/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.intrument.dio_panel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.TextView;
import android.util.Log;
import android.os.Handler;
import android.os.Looper;

import org.madresistor.box0.module.Dio;
import org.madresistor.box0.ResultException;
import org.madresistor.box0_studio.R;
import org.madresistor.box0_studio.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter instance is used to synchronise between different different thread
 *   that update the m_entries or use m_dio
 */

public class Adapter extends BaseAdapter
		implements AdapterView.OnItemClickListener,
					AdapterView.OnItemLongClickListener {
	private static final String TAG = Adapter.class.getName();
	private final LayoutInflater m_inflater;
	private final List<Entry> m_entries = new ArrayList<Entry>();
	private final String m_fmt_unnamed;
	private Dio m_dio;
	private InputPinReader m_inputPinReader;

	public class Entry {
		public String name;
		public boolean hiz;
		public boolean output;
		public boolean value;
	}

	private final Context m_context;
	public Adapter(Context context) {
		m_context = context;
		m_inflater = LayoutInflater.from(context);

		m_fmt_unnamed = context.getString(R.string.dio_unnamed_channel);
	}

	public void setModule(Dio dio) {
		/* tell the reader thead to stop its execution */
		if (m_inputPinReader != null) {
			m_inputPinReader.interrupt();
			m_inputPinReader = null;
		}

		synchronized (this) {
			m_dio = dio;

			if (dio != null) {
				int prev_count = m_entries.size();
				int new_count = dio.count.value;

				for (int i = 0; i < new_count; i++) {
					String name;
					try {
						name = dio.label.get(i);
					} catch(Exception e) {
						name = String.format(m_fmt_unnamed, i);
					}

					/* dont loose entries that exists from previous device */
					if (i < prev_count) {
						Entry e = m_entries.get(i);
						e.name = name;
					} else {
						Entry e = new Entry();
						e.name = name;
						e.hiz = false; /* all pins in output mode initally */
						e.output = true;
						e.value = false;
						m_entries.add(e);
					}
				}

				/* remove extra entries */
				if (new_count < prev_count) {
					m_entries.subList(new_count, prev_count).clear();
				}

				/* apply the configuration on module */
				try {
					for (short i = 0; i < m_entries.size(); i++) {
						Entry e = m_entries.get((int) i);
						m_dio.setHiz(i, e.hiz);
						m_dio.setDir(i, e.output);
						if (e.output) {
							m_dio.setValue(i, e.value);
						}
					}
				} catch (ResultException e) {
					/* FIXME: show proper message to user */
					if (Config.WARN) Log.w(TAG, "failed to initalize pin");
					m_dio = null;
				}
			}
		}

		notifyDataSetChanged();

		if (m_dio != null) {
			/* get a new reader thread */
			m_inputPinReader = new InputPinReader();
		}
	}

	@Override
	public int getCount() {
		return m_entries.size();
	}

	@Override
	public Object getItem(int position) {
		return m_entries.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		return m_entries.get(position).output ? 0 : 1;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Entry e = m_entries.get(position);
		if (convertView == null) {
			int layout = e.output ?
				R.layout.dio_pin_output :
				R.layout.dio_pin_input;
			convertView = m_inflater.inflate(layout, parent, false);
		}

		Checkable checkable = (Checkable) convertView;
		checkable.setChecked(e.value);

		TextView textView = (TextView) convertView;
		textView.setText(e.name);

		/* if the pin is in HiZ, disable it */
		convertView.setEnabled(!e.hiz);

		return convertView;
	}

	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Entry e = m_entries.get(position);
		if (!e.output || e.hiz) {
			return;
		}

		boolean value = !e.value;

		try {
			m_dio.setValue((short) position, value);
		} catch (ResultException re) {
			/* FIXME: show proper message to user */
			if (Config.WARN) Log.w(TAG, "unable to set value to hardware: " + re);
			return;
		}

		e.value = value;
		Checkable v = (Checkable) view;
		v.setChecked(e.value);
	}

	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		Entry e = m_entries.get(position);
		EditDialog dialog = new EditDialog(m_context, this, e);
		dialog.show();
		return true;
	}

	private class InputPinReader extends Thread {
		public InputPinReader() {
			super("DIO Input Pin Reader");

			/* start immediately reading */
			start();
		}

		public void run() {
			try {
				while (!isInterrupted()) {
					readPins();

					/* update the pins value again after 100ms */
					try {
						sleep(100);
					} catch(InterruptedException e) {
						break;
					}
				}
			} catch (ResultException re) {
				/* something failed, tell the user and stop the thread. */
				/* FIXME: show proper message to user */
				if (Config.WARN) Log.w(TAG, "reading failed: " + re);
			}
		}

		/* use to place a update request on ui thread */
		private final Handler m_handler = new Handler(Looper.getMainLooper());
		private final Runnable m_notifier = new Runnable() {
			public void run() {
				notifyDataSetChanged();
			}
		};

		/**
		 * Update the pins value
		 */
		public void readPins() throws ResultException {
			boolean something_updated = false;

			synchronized (Adapter.this) {
				/* find pins that are in input mode, and read there value */
				for (int i = 0; i < m_entries.size() && !isInterrupted(); i++) {
					Entry e = m_entries.get(i);

					if (!e.hiz && !e.output) {
						/* read pin value */
						e.value = m_dio.getValue((short) i);
						something_updated = true;
					}
				}
			}

			if (something_updated) {
				m_handler.post(m_notifier);
			}
		}
	}

	public void updatePin(Entry entry, boolean hiz, boolean output) {
		short i = (short) m_entries.indexOf(entry);
		if (i == -1) {
			if (Config.WTF) Log.wtf(TAG, "pin not found in m_entries");
			return;
		}

		boolean notify = false;

		try {
			synchronized (this) {
				if (entry.hiz != hiz) {
					if (m_dio != null) {
						m_dio.setHiz(i, hiz);
					}

					notify = true;
					entry.hiz = hiz;
				}

				if (entry.output != output) {
					if (m_dio != null) {
						m_dio.setDir(i, output);
					}

					notify = true;
					entry.output = output;
				}
			}
		} catch (ResultException re) {
			/* FIXME: show proper message to user */
			if (Config.WARN) Log.w(TAG, "operation failed on pin: " + re);
		}

		if (notify) {
			notifyDataSetChanged();
		}
	}

	public void hizAll(boolean hiz) {
		try {
			synchronized (this) {
				for (int i = 0; i < m_entries.size(); i++) {
					if (m_dio != null) {
						m_dio.setHiz((short) i, hiz);
					}
					m_entries.get(i).hiz = hiz;
				}
			}
		} catch (ResultException re) {
			/* FIXME: show proper message to user */
			if (Config.WARN) Log.w(TAG, "operation failed on pin: " + re);
		}

		notifyDataSetChanged();
	}

	public boolean isAllHiz() {
		for (Entry e: m_entries) {
			if (!e.hiz) {
				return false;
			}
		}

		return true;
	}
}
