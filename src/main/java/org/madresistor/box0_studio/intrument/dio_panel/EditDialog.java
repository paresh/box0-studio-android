/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.intrument.dio_panel;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;

import org.madresistor.box0_studio.R;
import org.madresistor.box0_studio.intrument.dio_panel.Adapter;

public class EditDialog extends AlertDialog implements DialogInterface.OnClickListener {
	private final Adapter m_adapter;
	private final Adapter.Entry m_entry;
	private final SwitchCompat m_hiz;
	private final Spinner m_dir;

	/* ASSUMING: 0:Input, 1:Output */
	private final int SPINNER_DIR_INPUT = 0;
	private final int SPINNER_DIR_OUTPUT = 1;

	public EditDialog(Context context, Adapter adapter, Adapter.Entry entry) {
		super(context);
		m_adapter = adapter;
		m_entry = entry;

		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(R.layout.dio_panel_edit, null);
		setView(rootView);

		m_hiz = (SwitchCompat) rootView.findViewById(R.id.dio_panel_edit_hiz);

		m_dir = (Spinner) rootView.findViewById(R.id.dio_panel_edit_dir);

		m_hiz.setChecked(entry.hiz);

		m_dir.setSelection(entry.output ?
			SPINNER_DIR_OUTPUT : SPINNER_DIR_INPUT);

		setTitle(entry.name);

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.ok), this);

		/* negative button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.cancel), this);
	}

	public void onClick(DialogInterface dialog, int which) {
		if (which != DialogInterface.BUTTON_POSITIVE) {
			return;
		}

		boolean hiz = m_hiz.isChecked();
		boolean output = m_dir.getSelectedItemPosition() == SPINNER_DIR_OUTPUT;
		m_adapter.updatePin(m_entry, hiz, output);
	}
}
