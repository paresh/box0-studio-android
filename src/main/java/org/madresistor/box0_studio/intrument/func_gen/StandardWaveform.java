/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.intrument.func_gen;

import java.util.Random;

/**
 * Standard waveform
 * Using this call, anyone can compute pre-defined function values into array.
 * Currently available functions are
 *  - Saw Tooth
 *  - Sine
 *  - Triangular
 *  - Noise
 *  - Square
 *  - Constant
 */
public class StandardWaveform {
	public static interface Function {
		public double compute(double angle);
	}

	private static final double _PI =  Math.PI;
	private static final double _2_PI = 2 * _PI;
	private static final double _PI_2 = _PI / 2;
	private static final double _3_PI_2 = 3 * _PI_2;

	public static class Sine implements Function {
		public double compute(double angle) {
			return Math.sin(angle);
		}
	}

	private static class Triangular implements Function {
		public double compute(double angle) {
			angle %= _2_PI;

			if (angle < _PI_2) {
				return angle / _PI_2;
			} else if (angle < _3_PI_2) {
				return 2 - (angle / _PI_2);
			} else {
				return (angle / _PI_2) - 4;
			}
		}
	}

	private static class SawTooth implements Function {
		public double compute(double angle) {
			angle %= _2_PI;

			if (angle < _PI) {
				return angle / _PI;
			} else {
				return (angle / _PI) - 2;
			}
		}
	}

	private static class Noise implements Function {
		private final Random m_random = new Random();

		public double compute(double angle) {
			return (m_random.nextDouble() - 0.5) * 2;
		}
	}

	private static class Constant implements Function {
		public double compute(double angle) {
			return 0;
		}
	}

	private static class Square implements Function {
		public double compute(double angle) {
			angle %= _2_PI;
			return angle >= _PI ? -1 : 1;
		}
	}

	public final static Function TRIANGULAR = new Triangular();
	public final static Function SINE = new Sine();
	public final static Function SAW_TOOTH = new SawTooth();
	public final static Function CONSTANT = new Constant();
	public final static Function NOISE = new Noise();
	public final static Function SQUARE = new Square();

	private static double clamp(double value, double min, double max) {
		value = Math.max(value, min);
		value = Math.min(value, max);
		return value;
	}

	/**
	 * Function to array
	 * <p>User can pass max = +INFINITY and min = -INFINITY to evade the limit</p>
	 * <p>For some type of function, argument are useless.</p>
	 * @param func Function
	 * @param freq Frequency (1 / freq = dx)
	 * @param amplitude Amplitude
	 * @param offset Offset
	 * @param phase Phase
	 * @param min Minimum value that can be generated
	 * @param max Maximum value that can be generated
	 * @param[out] x X Values
	 * @param[out] y Y Values
	 * @param COUNT Number of points to generate
	 */
	public static void func_to_array(Function func,
			double freq, double amplitude, double offset, double phase,
			double min, double max, float[] x, float[] y, int COUNT) {

		for (int i = 0; i < COUNT; i++) {
			double angle = ((i * _2_PI) / COUNT) + phase /* unit: RADIAN */;
			double result = func.compute(angle);
			result = (result * amplitude) + offset;
			y[i] = (float) clamp(result, min, max);
			if (x != null) {
				x[i] = (float) (i / (COUNT * freq));
			}
		}
	}
}
