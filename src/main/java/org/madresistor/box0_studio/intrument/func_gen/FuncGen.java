/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.intrument.func_gen;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.widget.ViewSwitcher;
import android.widget.AdapterView;
import android.text.TextWatcher;
import android.text.Editable;
import android.os.Handler;
import android.os.Message;
import android.view.View.OnFocusChangeListener;

import org.madresistor.box0.Device;
import org.madresistor.box0.ResultException;
import org.madresistor.box0.module.Aout;
import org.madresistor.box0_studio.Config;
import org.madresistor.box0_studio.R;
import org.madresistor.box0_studio.extra.ProblemDialog;
import org.madresistor.box0_studio.intrument.Intrument;
import org.madresistor.box0_studio.extra.SettingsFragment;
import org.madresistor.box0_studio.extra.input_filter.FreqInputFilter;
import org.madresistor.box0_studio.extra.ImageWithTextAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.nio.ByteBuffer;

public class FuncGen extends Intrument {
	private final static String TAG = FuncGen.class.getName();
	private boolean m_running;
	private Aout m_box0Aout;
	private OnStatusListener m_onStatusListener;

	private Frontend m_frontend = new Frontend();

	/* Note: THis sequence should match the
	 *  string array "func_gen_std_waveform_title"
	 */
	private final StandardWaveform.Function[] STANDARD_FUNC_MAP =
						new StandardWaveform.Function[] {
		StandardWaveform.SINE,
		StandardWaveform.TRIANGULAR,
		StandardWaveform.SAW_TOOTH,
		StandardWaveform.SQUARE,
		StandardWaveform.CONSTANT,
		StandardWaveform.NOISE
	};

	public FuncGen(Context context) {
		super(context);
	}

	public void setOnStatusListener(Intrument.OnStatusListener onStatusListener) {
		m_onStatusListener = onStatusListener;
	}

	@Override
	public Frontend getFrontend() {
		return m_frontend;
	}

	private ArrayList<Short> bitsize_map = new ArrayList<Short>();
	private ArrayList<String> channel_map = new ArrayList<String>();

	public void setDevice(Device box0Device) {
		bitsize_map.clear();
		channel_map.clear();

		if (m_box0Aout != null) {
			try {
				m_box0Aout.close();
			} catch (ResultException e) {
				//sheh, just ignoring this
			}
		}

		m_box0Aout = null;
		if (box0Device == null) {
			/* nothing more */
			return;
		}

		try {
			/* get the first AOUT module */
			m_box0Aout = box0Device.aout(0);
			m_box0Aout.staticPrepare();

			for (Short b : m_box0Aout.bitsize) {
				bitsize_map.add(b);
			}

			/* channel map */
			int i = 0;

			/* those whose name provided */
			for (String l: m_box0Aout.label) {
				channel_map.add(l);
				i++;
			}

			/* those whose name not provided */
			for (; i < m_box0Aout.count.value; i++) {
				channel_map.add("CH " + Integer.toString(i));
			}

		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while initalizing AOUT " + e + ": " + e.getMessage());
			Activity activity = m_frontend.getActivity();
			if (activity != null) {
				int msg = R.string.aout_init_failed;
				Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
			}

			m_box0Aout = null;
		}

		m_frontend.updateModuleDependentInputs();

		setRunning(false);
	}

	public void stopAout() {
		if (m_box0Aout == null) {
			if (Config.WTF) Log.wtf(TAG, "box0Aout is null");
			/* TODO: show error to user */
			return;
		}

		try {
			m_box0Aout.staticStop();
		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while stopping AOUT" + e.getMessage() + ": " + e);
		}
	}

	/**
	 * Try to close the AOUT module
	 */
	private void closeAout() {
		if (m_box0Aout == null) {
			if (Config.DEBUG) Log.d(TAG, "no AOUT module to close");
			return;
		}

		try {
			m_box0Aout.close();
		} catch (ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while closing AOUT" + e.getMessage() + ": " + e);
		}

		m_box0Aout = null;
	}

	/**
	 * mark the module as running.
	 * This will also update the start/stop menu item
	 */
	private void setRunning(boolean running) {
		/* mark as not running */
		m_running = running;
		m_frontend.syncStartStop();
	}

	public void freeResource() {
		stopAout();
		closeAout();
		m_frontend.updateModuleDependentInputs();
		setRunning(false);
	}

	private enum RunState {
		DONT_CHANGE, /* only graph is update */
		RESTART_IF_RUNNING, /* graph updated and only restart if running */
		RUN_ANYWAY /* graph updated and start if stopped, restart if running */
	};

	private void waveformProcess(WaveformParam param) {
		if (Config.DEBUG) {
			Log.d(TAG, "param.custom: " + param.custom);
			String std_func_name = (param.std_func != null) ?
				param.std_func.getClass().getName() :
				"NULL";
			Log.d(TAG, "param.std_func: " + std_func_name);
			Log.d(TAG, "param.std_freq: " + param.std_freq);
			Log.d(TAG, "param.std_amp: " + param.std_amp);
			Log.d(TAG, "param.std_offset: " + param.std_offset);
			Log.d(TAG, "param.std_phase: " + param.std_phase);
			Log.d(TAG, "param.custom_equation: " + param.custom_equation);
			Log.d(TAG, "param.custom_count: " + param.custom_count);
			Log.d(TAG, "param.custom_speed: " + param.custom_speed);
			Log.d(TAG, "param.max: " + param.max);
			Log.d(TAG, "param.min: " + param.min);
			Log.d(TAG, "param.run_state: " + param.run_state);
			Log.d(TAG, "param.channel: " + param.channel);
			Log.d(TAG, "param.bitsize: " + param.bitsize);
			Log.d(TAG, "param.repeat: " + param.repeat);
			Log.d(TAG, "param.ignore_parser_error: " + param.ignore_parser_error);
		}

		new WaveformProces().execute(param);
	}

	private static class WaveformParam {
		public boolean custom;

		/* standard */
		public StandardWaveform.Function std_func;
		public double std_freq;
		public double std_amp;
		public double std_offset;
		public double std_phase; /* radian */

		/* custom */
		public String custom_equation;
		public long custom_count;
		public long custom_speed;

		/* common for standard and custom */
		public double max;
		public double min;

		/* only used in StartAout */
		public RunState run_state;
		public short channel;
		public short bitsize;
		public long repeat; /* = 0 if input empty */

		public boolean ignore_parser_error;
	}

	private static class WaveformResult {
		/* graph data */
		public float[] x, y;
		boolean update_running, running;

		public static class Error {
			/* description if error occured */
			public final String title;
			public final String desc;

			public Error(String t, String d) {
				title = t;
				desc = d;
			}

			public Error(Context context, int t, String d) {
				Resources res = context.getResources();
				title = res.getString(t);
				desc = d;
			}

			public Error(Context context, int t, int d) {
				Resources res = context.getResources();
				title = res.getString(t);
				desc = res.getString(d);
			}
		};

		/* description if error occured */
		public Error error;
	}

	private class WaveformProces extends AsyncTask<WaveformParam, Void, WaveformResult> {

		/**
		 * synchronise graph with equation in @a param and store store data in @a result
		 * @param param Paramter
		 * @param result Result
		 * @return true on success
		 * @return false on failure
		 */
		private boolean syncGraph(WaveformParam param, WaveformResult result) {
			final int COUNT = 300;
			float[] x = new float[COUNT];
			float[] y = new float[COUNT];

			if (param.custom) {
				try {
					equation_to_array(param.custom_equation,
						param.custom_speed, param.min, param.max, x, y, COUNT);
				} catch (RuntimeException e) {
					/* store the error */
					if (Config.DEBUG) Log.d(TAG, "equation calculation problem: " + e.getMessage());
					if (!param.ignore_parser_error) {
						result.error = new WaveformResult.Error(
								m_frontend.getActivity(),
								R.string.equation_error,
								e.getMessage());
					}
					return false;
				}
			} else {
				StandardWaveform.func_to_array(param.std_func,
					param.std_freq, param.std_amp,
					param.std_offset, param.std_phase,
					param.min, param.max, x, y, COUNT);
			}

			result.x = x;
			result.y = y;
			return true;
		}

		/**
		 * Start Analog out module
		 * @param param Paramter
		 * @param result Result
		 * @return true on success
		 * @return false on failure
		 */
		private boolean startAout(WaveformParam param, WaveformResult result) {
			long speed;
			int count;

			m_sharedHandler.stopRepeatTimeout();

			if (param.custom) {
				speed = param.custom_speed;
				count = (int) param.custom_count;
			} else {
				/* The more the buffer on device utilized and
				 * the lower the speed will generate better result
				*/
				Aout.StaticCalc calc;
				try {
					calc = m_box0Aout.staticCalc(param.std_freq, param.bitsize);
				} catch (ResultException e) {
					if (Config.DEBUG) Log.d(TAG, "unable to find any configuration (speed, buffer-size)");
					result.error = new WaveformResult.Error(
						m_frontend.getActivity(),
						R.string.aout_configuration_failed,
						R.string.aout_no_favourable_speed_and_buffer_size);
					return false;
				}

				if (Config.DEBUG) Log.d(TAG, "calc.count:" + calc.count);
				if (Config.DEBUG) Log.d(TAG, "calc.speed:" + calc.speed);
				speed = calc.speed;
				count = (int) calc.count;
			}

			float[] waveform = new float[count];

			if (param.custom) {
				try {
					equation_to_array(param.custom_equation,
						param.custom_speed, param.min, param.max,
						null, waveform, count);
				} catch (RuntimeException e) {
					/* store the error */
					if (Config.DEBUG) Log.d(TAG, "equation calculation problem: " + e.getMessage());
					if (!param.ignore_parser_error) {
						result.error = new WaveformResult.Error(
							m_frontend.getActivity(),
							R.string.equation_error, e.getMessage());
					}
					return false;
				}
			} else {
				StandardWaveform.func_to_array(param.std_func,
					param.std_freq, param.std_amp,
					param.std_offset, param.std_phase,
					param.min, param.max,
					null, waveform, count);
			}

			/* configure, send data to device and start */
			try {
				m_box0Aout.repeat.set(param.repeat);
				m_box0Aout.bitsize.set(param.bitsize);
				m_box0Aout.speed.set(speed);
				m_box0Aout.chanSeq.set(new short [] { param.channel });
				m_box0Aout.staticStart(waveform);
			} catch(ResultException e) {
				if (Config.DEBUG) Log.d(TAG, "failed to start module: " + e.getMessage());
				result.error = new WaveformResult.Error(
						m_frontend.getActivity(),
						R.string.aout_start_failed,
						R.string.aout_start_failed_desc);
				return false;
			}

			/* if this is repeat (for finite time) stop after the waveform is over */
			if (param.repeat > 0) {
				double dt = 1.0 / speed;
				double dur = param.repeat * count * dt;
				m_sharedHandler.startRepeatTimeout(dur);
			}

			return true;
		}

		protected WaveformResult doInBackground(WaveformParam... params) {
			WaveformParam param = params[0];
			WaveformResult result = new WaveformResult();
			result.update_running =
				param.run_state == RunState.RUN_ANYWAY ||
				(m_running && param.run_state == RunState.RESTART_IF_RUNNING);

			/* stop module (if start will be happening) */
			if (m_running && result.update_running) {
				result.running = false;
				stopAout();
			}

			/* update graph */
			if (!syncGraph(param, result)) {
				return result;
			}

			/* start module if requested */
			if (result.update_running && param.bitsize != -1 && param.channel != -1) {
				result.running = startAout(param, result);
				if (!result.running) {
					/* failed to run */
					return result;
				}
			}

			/* success */
			return result;
		}

		protected synchronized void onPostExecute(WaveformResult result) {
			if (result.error != null) {
				/* show the error */
				new ProblemDialog(m_frontend.getActivity(),
					result.error.title, result.error.desc);
			}

			m_frontend.setPlotData(result.x, result.y);

			if (result.update_running) {
				setRunning(result.running);
			}
		}
	}

	private class SharedHandler extends Handler {
		private static final int REPEAT_TIMEOUT = 1;

		/** Hold back for a finite duration
		 *  so that, it give time to user to input data full */
		private static final int INPUT_CHANGED = 2;

		/** Number of milliseconds for wait to user to enter data */
		private static final long INPUT_CHANGED_DEADTIME = 300;

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == REPEAT_TIMEOUT) {
				/* Repeat timeout occured, we need to mark as stopped */
				setRunning(false);
			} else if (msg.what == INPUT_CHANGED) {
				/* With the new inputs,
				 *  - update graph
				 *  - if the module is running, restart it
				 */
				WaveformParam param = m_frontend.prepareWaveformParam();
				if (param != null) {
					param.run_state = RunState.RESTART_IF_RUNNING;
					waveformProcess(param);
				}
			}
		}

		/**
		 * Abstracted frontend to repeat timeout start
		 * @param dur_ms Duration in seconds
		 */
		public void startRepeatTimeout(double dur) {
			stopRepeatTimeout(); /* stop any previous repeat timeout */
			long dur_ms = (long) dur * 1000; /* to milliseconds */
			if (!sendEmptyMessageDelayed(REPEAT_TIMEOUT, dur_ms)) {
				if (Config.WARN) Log.w(TAG, "unable to send empty message REPEAT_TIMEOUT in shared handler");
			}
		}

		/**
		 * Abstracted frontend to repeat timeout stop
		 */
		public void stopRepeatTimeout() {
			removeMessages(REPEAT_TIMEOUT);
		}

		/**
		 * Abstract frontend to deadtime for user to enter full details
		 */
		public void userChangedInput() {
			removeMessages(INPUT_CHANGED); /* user changed more, reset the counter */
			if (!sendEmptyMessageDelayed(INPUT_CHANGED, INPUT_CHANGED_DEADTIME)) {
				if (Config.WARN) Log.w(TAG, "unable to send empty message INPUT_CHANGED in shared handler");
			}
		}
	}

	private final SharedHandler m_sharedHandler = new SharedHandler();

	private class Frontend extends Intrument.Frontend
			implements AdapterView.OnItemSelectedListener, TextWatcher {
		private final String TAG = Frontend.class.getName();

		private Plot m_plot;

		private Spinner m_channel = null;
		private Spinner m_bitsize = null;
		private EditText m_repeat = null;

		private MenuItem m_actionStartStop = null;
		private MenuItem m_actionCustom = null;

		private ViewSwitcher m_viewSwitcher = null;

		/* standard */
		private Spinner m_std_waveform = null;
		private EditText m_std_offset = null;
		private EditText m_std_amp = null;
		private EditText m_std_freq = null;
		private EditText m_std_phase = null;

		/* custom */
		private EditText m_custom_eq = null;
		private EditText m_custom_speed = null;
		private EditText m_custom_count = null;

		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		private GT0AndLTEMaxInputFilter m_customSpeedInputFilter =
			new GT0AndLTEMaxInputFilter();

		private GT0AndLTEMaxInputFilter m_customCountInputFilter =
			new GT0AndLTEMaxInputFilter();

		/**
		 * Set some data on the plot
		 * @param x X values
		 * @param y Y values
		 * @note pass @a x = null and @a y = null for clearing plot
		 */
		private void setPlotData(float[] x, float[] y) {
			if (m_plot == null) {
				if (Config.DEBUG) Log.d(TAG, "unable to set data because m_plot is null");
				return;
			}

			m_plot.setData(x, y);
		}

		/**
		 * Synchronise "m_running" and start/stop menu item
		 */
		private void syncStartStop() {
			if (m_actionStartStop == null) {
				if (Config.WARN) Log.w(TAG, "m_actionStartStop is null");
				return;
			}

			int string;
			int icon;

			if (m_running) {
				icon = R.drawable.ic_stop_grey600_48dp;
				string = R.string.stop;
			} else {
				icon = R.drawable.ic_play_grey600_48dp;
				string = R.string.start;
			}

			m_actionStartStop.setTitle(string);
			m_actionStartStop.setIcon(icon);
		}

		/**
		 * Update the maximum value of count in custom input
		 * @param bitsize For the given bitsize
		 * @note if @a bitsize == 0, custom count filtering is disabled
		 */
		private void updateCustomCountValue(short bitsize) {
			if (m_box0Aout == null || bitsize == 0) {
				m_customCountInputFilter.setMaximum(0);
				return;
			}

			short bytesize = (short) ((bitsize + 7) / 8);
			long value = m_box0Aout.buffer.value / bytesize;
			m_customCountInputFilter.setMaximum(value);

			if (m_custom_count != null) {
				m_custom_count.setText(Long.toString(value));
			}
		}

		/**
		 * Update the maximum custom count that can be used.
		 * @note use current bitsize value
		 */
		private void updateCustomCountValue() {
			if (m_bitsize == null) {
				updateCustomCountValue((short) 0);
				return;
			}

			int bs_i = m_bitsize.getSelectedItemPosition();
			if (bs_i >= 0 && bs_i < bitsize_map.size()) {
				updateCustomCountValue(bitsize_map.get(bs_i));
			} else {
				updateCustomCountValue((short) 0);
			}
		}

		/**
		 * Update the maximum speed that can be used in custom equation
		 */
		private void updateCustomSpeedValue() {
			if (m_box0Aout == null) {
				m_customCountInputFilter.setMaximum(0);
				return;
			}

			/* find the maximum speed */
			long max = getMaximumSpeed(m_box0Aout.speed.getPointer());
			m_customSpeedInputFilter.setMaximum(max);

			if (m_custom_speed != null) {
				m_custom_speed.setText(Long.toString(max));
			}
		}

		/**
		 * Update the list of available channels that can be selected by user
		 * The list is generated from channel_map.
		 * This is used when either a new device has been connected or
		 *   layout is inflated.
		 */
		private void populateChannel() {
			if (m_channel == null) {
				if (Config.WARN) Log.w(TAG, "m_channel is null");
				return;
			}

			Activity activity = getActivity();
			if (activity == null) {
				if (Config.WARN) Log.w(TAG, "activity is null");
				return;
			}

			ArrayAdapter adapter = new ArrayAdapter<String> (activity,
				android.R.layout.simple_spinner_item, channel_map);
			adapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item);
			m_channel.setAdapter(adapter);
		}

		/**
		 * Update the list of bitsize that can be selected by user.
		 * The list is generated from bitsize_map
		 * This used when either a new device has been connected or
		 *   layout is inflated
		 */
		private void populateBitsize() {
			if (m_bitsize == null) {
				if (Config.WARN) Log.w(TAG, "m_bitsize is null");
				return;
			}

			Activity activity = getActivity();
			if (activity == null) {
				if (Config.WARN) Log.w(TAG, "acitvity is null");
				return;
			}

			ArrayList<String> list = new ArrayList<String>();
			for (Short b: bitsize_map) {
				list.add(b.toString() + " bits");
			}

			ArrayAdapter adapter = new ArrayAdapter<String> (activity,
				android.R.layout.simple_spinner_item, list);
			adapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item);
			m_bitsize.setAdapter(adapter);
		}

		/**
		 * Called when a device has changed or view is inflated.
		 *  It change include new device got added or device got removed.
		 */
		public void updateModuleDependentInputs() {
			populateChannel();
			populateBitsize();
			updateCustomSpeedValue();
			updateCustomCountValue();
		}

		private static final int WAVEFORM_SINE_SPINNER_INDEX = 0;
		private static final int WAVEFORM_TRIANGULAR_SPINNER_INDEX = 1;
		private static final int WAVEFORM_SAW_TOOTH_SPINNER_INDEX = 2;
		private static final int WAVEFORM_SQUARE_SPINNER_INDEX = 3;
		private static final int WAVEFORM_CONSTANT_SPINNER_INDEX = 4;
		private static final int WAVEFORM_NOISE_SPINNER_INDEX = 5;

		/**
		 * Enable standard input as per waveform index
		 *  for a standard waveform, some inputs are useless.
		 * Example: if DC is selected, there is no requirement of frequency
		 * @param index Standard waveform index
		 */
		private void enableStandardInputAsPerWaveform(int index) {
			boolean amp = false, phase  = false, freq = false, offset = false;

			switch (index) {
			case WAVEFORM_SINE_SPINNER_INDEX:
			case WAVEFORM_TRIANGULAR_SPINNER_INDEX:
			case WAVEFORM_SAW_TOOTH_SPINNER_INDEX:
			case WAVEFORM_SQUARE_SPINNER_INDEX:
				amp = true;
				phase = true;
				freq = true;
				offset = true;
			break;
			case WAVEFORM_CONSTANT_SPINNER_INDEX:
				offset = true;
			break;
			case WAVEFORM_NOISE_SPINNER_INDEX:
				offset = true;
				amp = true;
			break;
			default:
			break;
			}

			if (m_std_amp != null) m_std_amp.setEnabled(amp);
			if (m_std_freq != null) m_std_freq.setEnabled(freq);
			if (m_std_phase != null) m_std_phase.setEnabled(phase);
			if (m_std_offset != null) m_std_offset.setEnabled(offset);
		}

		/**
		 * Enable standard input as per the selected standard waveform
		 * On a valid index, pass that on to
		 *  {@a #enableStandardInputAsPerWaveform(int index)}
		 */
		private void enableStandardInputAsPerWaveform() {
			if (m_std_waveform == null) {
				if (Config.DEBUG) Log.d(TAG, "m_std_waveform is null");
				return;
			}

			int index = m_std_waveform.getSelectedItemPosition();
			if (index == -1) {
				if (Config.DEBUG) Log.d(TAG, "there are no selected item in m_std_waveform");
				return;
			}

			enableStandardInputAsPerWaveform(index);
		}

		/**
		 * Populate the standard waveform list
		 */
		private void populateStandardWaveform() {
			if (m_std_waveform == null) {
				if (Config.WARN) Log.w(TAG, "m_std_waveform is null");
				return;
			}

			ImageWithTextAdapter<CharSequence> std_waveform_adapter =
				ImageWithTextAdapter.createFromResource(getActivity(),
					R.array.func_gen_std_waveform_icon,
					R.array.func_gen_std_waveform_title,
					R.id.image1, R.id.text1,
					R.layout.image_with_text_spinner_item);
			//std_waveform_adapter.setDropDownViewResource(
			//	R.layout.image_with_text_spinner_dropdown_item);
			m_std_waveform.setAdapter(std_waveform_adapter);
		}

		/**
		 * Parepare the @a param object with standard waveform inputs.
		 * @return null if input error/empty
		 * @return @a param on success
		 */
		public WaveformParam prepareWaveformParamStandard(WaveformParam param) {
			if (m_std_waveform == null || m_std_offset == null ||
				m_std_amp == null || m_std_phase == null || m_std_freq == null) {
				if (Config.DEBUG) Log.d(TAG, "m_std_waveform or m_std_offset or m_std_amp or m_std_phase or m_std_freq is null");
				return null;
			}

			int func_index = m_std_waveform.getSelectedItemPosition();
			if (func_index == -1) {
				if (Config.DEBUG) Log.d(TAG, "currently selected standard waveform is invalid");
				return null;
			}

			param.std_func = STANDARD_FUNC_MAP[func_index];

			/* extract offset
			 * UI Note: if user entered nothing, assume 0
			 */
			String offsetText = m_std_offset.getText().toString();
			try {
				param.std_offset = (offsetText.length() < 1)
					? 0 : Double.parseDouble(offsetText);
			} catch (NumberFormatException nfe) {
				 if (Config.WARN) Log.d(TAG, "standard offset parse problem: " + nfe.getMessage());
				return null;
			}

			/* extract amplitude */
			String amplitudeText = m_std_amp.getText().toString();
			if (amplitudeText.length() < 1) {
				if (Config.DEBUG) Log.d(TAG, "standard amplitude string empty");
				return null;
			}
			try {
				param.std_amp = Double.parseDouble(amplitudeText);
			} catch (NumberFormatException nfe) {
				if (Config.WARN) Log.d(TAG, "standard amplitude parse problem: " + nfe.getMessage());
				return null;
			}

			/* extract phase
			 * UI Note: if phase has nothing assume 0
			 */
			String phaseText = m_std_phase.getText().toString();
			try {
				param.std_phase = (phaseText.length() < 1) ?
					0 : Double.parseDouble(phaseText);
			} catch (NumberFormatException nfe) {
				if (Config.WARN) Log.d(TAG, "standard phase parse problem: " + nfe.getMessage());
				return null;
			}
			/* input from user is taken in degree
			 *  because it is easier to understand (atleast for me :p) */
			param.std_phase = Math.toRadians(param.std_phase);

			/* extract freq */
			String freqText = m_std_freq.getText().toString();
			if (freqText.length() < 1) {
				if (Config.DEBUG) Log.d(TAG, "standard freq string empty");
				return null;
			}
			try {
				param.std_freq =  Double.parseDouble(freqText);
			} catch (NumberFormatException nfe) {
				if (Config.WARN) Log.d(TAG, "standard frequency parse problem: " + nfe.getMessage());
				return null;
			}

			return param;
		}

		/**
		 * Parepare the @a param object with custom equation inputs.
		 * @return null if input error/empty
		 * @return @a param on success
		 */
		public WaveformParam prepareWaveformParamCustom(WaveformParam param) {
			if (m_custom_eq == null || m_custom_speed == null ||
				m_custom_count == null) {
				if (Config.DEBUG) Log.d(TAG, "m_custom_eq or m_custom_speed or m_custom_count is null");
				return null;
			}

			String equation = m_custom_eq.getText().toString();
			if (equation.length() < 1) {
				if (Config.DEBUG) Log.d(TAG, "custom equation string empty");
				return null;
			}
			param.custom_equation = equation;

			String speedText = m_custom_speed.getText().toString();
			if (speedText.length() < 1) {
				if (Config.DEBUG) Log.d(TAG, "custom freq string empty");
				return null;
			}
			try {
				param.custom_speed = Long.parseLong(speedText);
			} catch (NumberFormatException nfe) {
				if (Config.WARN) Log.d(TAG, "custom speed parse problem: " + nfe.getMessage());
				return null;
			}

			try {
				String countText = m_custom_count.getText().toString();
				if (countText.length() < 1) {
					if (Config.DEBUG) Log.d(TAG, "custom count string empty");
					return null;
				}
				param.custom_count = Long.parseLong(countText);
			} catch (NumberFormatException nfe) {
				if (Config.WARN) Log.d(TAG, "custom count parse problem: " + nfe.getMessage());
				return null;
			}

			/* ignore parser error when user is editing equation */
			if (m_userEditingCustomEquation && Config.DEBUG) Log.d(TAG, "user is editing equation, will ignore parser error");
			param.ignore_parser_error = m_userEditingCustomEquation;

			return param;
		}

		/**
		 * Get waveform parameter
		 * @return null if inputs missing OR error occured OR view not available
		 * @return instance of WaveformParam if success
		 */
		public WaveformParam prepareWaveformParam() {
			if (m_repeat == null || m_channel == null || m_bitsize == null || m_viewSwitcher == null) {
				if (Config.DEBUG) Log.d(TAG, "m_repeat or m_channel or m_bitsize or m_viewSwitcher is null");
				return null;
			}

			WaveformParam param = new WaveformParam();

			if (m_box0Aout == null) {
				/* if their is no module, let the user play with plot.
				 * make maximum value possible
				 */
				param.max = Double.POSITIVE_INFINITY;
				param.min = Double.NEGATIVE_INFINITY;
			} else {
				param.max = m_box0Aout.ref.high;
				param.min = m_box0Aout.ref.low;
			}

			String repeatText = m_repeat.getText().toString();
			try {
				/* assume 0 if not provided */
				param.repeat = (repeatText.length() < 1) ?
					0 : Long.parseLong(repeatText);
			} catch (NumberFormatException nfe) {
				if (Config.WARN) Log.d(TAG, "repeat parse error: " + nfe.getMessage());
				return null;
			}

			int ch_index = m_channel.getSelectedItemPosition();
			if (ch_index == -1) {
				if (Config.DEBUG) Log.d(TAG, "m_channel have no selected item");
			}
			param.channel = (short) ch_index;

			int bs_i = m_bitsize.getSelectedItemPosition();
			if (bs_i >= 0 && bs_i < bitsize_map.size()) {
				param.bitsize = bitsize_map.get(bs_i);
			} else {
				if (Config.DEBUG) Log.d(TAG, "m_bitsize have no selected item");
				param.bitsize = (short) -1;
			}

			param.custom = m_viewSwitcher.getDisplayedChild() > 0;

			return (param.custom) ?
					prepareWaveformParamCustom(param) :
					prepareWaveformParamStandard(param);
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			if (Config.DEBUG) Log.d(TAG, "inside onCreateOptionsMenu");
			super.onCreateOptionsMenu(menu, inflater);

			inflater.inflate(R.menu.func_gen_actions, menu);
			m_actionStartStop = menu.findItem(R.id.func_gen_action_start_stop);
			m_actionCustom = menu.findItem(R.id.func_gen_action_custom);
		}

		/***
		 * Called when invalidateOptionsMenu() is triggered
		 */
		@Override
		public void onPrepareOptionsMenu(Menu menu) {
			if (Config.DEBUG) Log.d(TAG, "inside onPrepareOptionsMenu");
			super.onPrepareOptionsMenu(menu);

			/* update the start stop action */
			m_actionStartStop.setVisible(m_box0Aout != null);
			syncStartStop();

			if (m_viewSwitcher != null) {
				boolean custom = m_viewSwitcher.getDisplayedChild() > 0;
				m_actionCustom.setChecked(custom);
			} else {
				if (Config.DEBUG) Log.d(TAG, "m_viewSwitcher is null, unable to initalize m_actionCustom");
			}
		}

		/**
		 * Change the running state to @a running
		 * This is called when user press the play/stop button
		 */
		private void userWantToSwitchRunning(boolean running) {
			if (m_running) {
				m_sharedHandler.stopRepeatTimeout(); /* stop any repeat timeout timer */
				stopAout(); /* stop the actual module */
				setRunning(false); /* update the UI */

				/* return if we are only told to stop? */
				if (!running) {
					return;
				}
			}

			WaveformParam param = prepareWaveformParam();
			if (param == null) {
				if (Config.DEBUG) Log.d(TAG, "cannot start because some inputs are missing");
				new ProblemDialog(getActivity(),
					R.string.func_gen_input_missing_title,
					R.string.func_gen_input_missing_desc);
				return;
			}

			param.run_state = RunState.RUN_ANYWAY;
			waveformProcess(param);
		}

		/**
		 * Called when user want to switch to/from custom equation
		 * @param custom Show custom equation if true
		 */
		private void userWantToSwitchCustom(boolean custom) {
			m_viewSwitcher.setDisplayedChild(custom ? 1 : 0);

			/* force stop when user change to custom mode
			 *      and inputs are empty */
			m_sharedHandler.userChangedInput();
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			if (Config.DEBUG) Log.d(TAG, "inside onOptionsItemSelected");

			switch(item.getItemId()) {
			case R.id.func_gen_action_start_stop:
				if (Config.DEBUG) Log.d(TAG, m_running ?
					"User want to stop module" :
					"User want to start module");
				userWantToSwitchRunning(!m_running);
			return true;
			case R.id.func_gen_action_custom:
				boolean change_to = !item.isChecked();
				if (Config.DEBUG) Log.d(TAG, change_to ?
					"User want to switch to custom equation":
					"User want to switch to standard waveform");
				item.setChecked(change_to);
				userWantToSwitchCustom(change_to);
			return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}

		@Override
		public void onDestroyOptionsMenu() {
			if (Config.DEBUG) Log.d(TAG, "inside onDestroyOptionsMenu");
			super.onDestroyOptionsMenu();

			m_actionStartStop = null;
			m_actionCustom = null;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
							Bundle savedInstanceState) {
			if (Config.DEBUG) Log.d(TAG, "inside onCreateView");
			return inflater.inflate(R.layout.fragment_func_gen, container, false);
		}

		/**
		 * Initalize the graph.
		 */
		private void initGraph() {
			Activity activity = getActivity();
			SharedPreferences sharedPref =
				PreferenceManager.getDefaultSharedPreferences(activity);

			/* set line width */
			int lineWidth = sharedPref.getInt(
				SettingsFragment.PREF_LINE_WIDTH,
				SettingsFragment.DEF_LINE_WIDTH);

			m_plot.setLineWidth(lineWidth);
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			if (Config.DEBUG) Log.d(TAG, "inside onViewCreated");
			super.onViewCreated(view, savedInstanceState);

			m_viewSwitcher = (ViewSwitcher) view.findViewById(R.id.func_gen_view_switcher);

			m_plot = (Plot) view.findViewById(R.id.func_gen_output);

			m_channel = (Spinner) view.findViewById(R.id.func_gen_channel);
			m_bitsize = (Spinner) view.findViewById(R.id.func_gen_bitsize);
			m_repeat = (EditText) view.findViewById(R.id.func_gen_repeat);

			m_std_waveform = (Spinner) view.findViewById(R.id.func_gen_std_waveform);
			m_std_offset = (EditText) view.findViewById(R.id.func_gen_std_offset);
			m_std_amp = (EditText) view.findViewById(R.id.func_gen_std_amp);
			m_std_freq = (EditText) view.findViewById(R.id.func_gen_std_freq);
			m_std_phase = (EditText) view.findViewById(R.id.func_gen_std_phase);

			m_custom_eq = (EditText) view.findViewById(R.id.func_gen_custom_eq);
			m_custom_speed = (EditText) view.findViewById(R.id.func_gen_custom_speed);
			m_custom_count = (EditText) view.findViewById(R.id.func_gen_custom_count);

			m_std_freq.setFilters(new InputFilter [] {
				new FreqInputFilter()
			});

			m_custom_speed.setFilters(new InputFilter [] {
				m_customSpeedInputFilter
			});

			m_custom_count.setFilters(new InputFilter [] {
				m_customCountInputFilter
			});

			populateStandardWaveform();
			updateModuleDependentInputs();

			initGraph();
		}

		@Override
		public void onResume() {
			if (Config.DEBUG) Log.d(TAG, "inside onResume");
			super.onResume();

			/* why post on a runnable instead of directly doing the stuff in onResume.
			 * Because: Unwanted callback from
			 *  - spinner [item selected]
			 *  - EditText [text changed
			 * This prevent unwanted callback for the first time.
			 */
			m_sharedHandler.post(new Runnable() {
				@Override
				public void run() {
					enableStandardInputAsPerWaveform();
					/* Note: ^^^^ should have been called
					 *  from onViewStateRestored() but did not work.
					 *  Inputs where not enabled for the crossponding waveform.
					 *  probebly the spinner was not giving correct selected index
					 */

					m_std_waveform.setOnItemSelectedListener(Frontend.this);
					m_channel.setOnItemSelectedListener(Frontend.this);
					m_bitsize.setOnItemSelectedListener(Frontend.this);

					m_repeat.addTextChangedListener(Frontend.this);
					m_std_offset.addTextChangedListener(Frontend.this);
					m_std_amp.addTextChangedListener(Frontend.this);
					m_std_freq.addTextChangedListener(Frontend.this);
					m_std_phase.addTextChangedListener(Frontend.this);
					m_custom_eq.addTextChangedListener(Frontend.this);
					m_custom_speed.addTextChangedListener(Frontend.this);
					m_custom_count.addTextChangedListener(Frontend.this);

					m_custom_eq.setOnFocusChangeListener(m_customEquationFocusChangeListener);

					WaveformParam param = prepareWaveformParam();
					if (param != null) {
						param.run_state = RunState.DONT_CHANGE;
						waveformProcess(param);
					}
				}
			});
		}

		@Override
		public void onPause() {
			if (Config.DEBUG) Log.d(TAG, "inside onPause");
			super.onPause();

			m_std_waveform.setOnItemSelectedListener(null);
			m_channel.setOnItemSelectedListener(null);
			m_bitsize.setOnItemSelectedListener(null);

			m_repeat.removeTextChangedListener(this);
			m_std_offset.removeTextChangedListener(this);
			m_std_amp.removeTextChangedListener(this);
			m_std_freq.removeTextChangedListener(this);
			m_std_phase.removeTextChangedListener(this);
			m_custom_eq.removeTextChangedListener(this);
			m_custom_speed.removeTextChangedListener(this);
			m_custom_count.removeTextChangedListener(this);

			m_custom_eq.setOnFocusChangeListener(null);
		}

		@Override
		public void onDestroyView() {
			if (Config.DEBUG) Log.d(TAG, "inside onDestroyView");
			super.onDestroyView();

			m_channel = null;
			m_bitsize = null;
			m_repeat = null;

			m_std_waveform = null;
			m_std_offset = null;
			m_std_amp = null;
			m_std_freq = null;
			m_std_phase = null;

			m_custom_eq = null;
			m_custom_speed = null;
			m_custom_count = null;
		}

		/**
		 * Spinner callbacks when user selected an item
		 *  - channel
		 *  - bitsize
		 *  - standard waveform
		 * @param parent Spinner
		 * @param view Item View
		 * @param position Position of @a View
		 * @param id ID generated from the position
		 */
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			if (parent == m_std_waveform) {
				enableStandardInputAsPerWaveform(position);
			} else if (parent == m_bitsize) {
				updateCustomCountValue();
			}

			m_sharedHandler.userChangedInput();
		}

		public void onNothingSelected(AdapterView<?> parent) {}

		/**
		 * When Custom equation is being edited,
		 *   m_userEditingCustomEquation is true.
		 * This is used to make sure that (only) parsing error's are
		 *  not showed when user is typing equation.
		 */
		private boolean m_userEditingCustomEquation = false;
		private OnFocusChangeListener m_customEquationFocusChangeListener =
						new OnFocusChangeListener() {
			public void onFocusChange(View view, boolean focused) {
				m_userEditingCustomEquation = focused;
			}
		};

		/**
		 *  callback when user change some input (EditText)
		 *  - standard {amplitude, offset, phase, frequency}
		 *  - custom {equation, count, speed}
		 *  - repeat
		 */
		public void afterTextChanged(Editable s) {
			m_sharedHandler.userChangedInput();
		}
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		public void onTextChanged(CharSequence s, int start, int before, int count) {}
	}

	/**
	 * Convert equation into values (using MuParser)
	 * Written in C because of effeciency reason
	 * @param equation Equation to use
	 * @param freq Sampling Frequency
	 * @param min Minimum value that can be generated
	 * @param max Maximum value that can be generated
	 * @param x X values (can be null - if only Y values are required)
	 * @param y Y values (*cannot* be null)
	 * @param COUNT number of point to store in @a x and @a y
	 * @note values will always be between @a max and @a min
	 * @throws RuntimeException when parsing error occur containing the message
	 *
	 * @note
	 * Constants:
	 *  PI = 3.14...
	 *  EN = Eular Number
	 *  COUNT = @a COUNT
	 *  FREQ = @a freq
	 *
	 * Variables:
	 *  i = Index variable
	 *  x = X axis value at "i"
	 */
	private native static void equation_to_array(String equation,
		double freq, double min, double max,
		float[] /* only for graph */ x, float[] /* graph and device */ y,
		int COUNT) throws RuntimeException;

	/**
	 * get the maximum speed of the b0_speed
	 * Written in C because of effeciency reason
	 * @param pointer pointer to b0_speed property
	 * @return maximum value from pointer->values
	 */
	private native long getMaximumSpeed(ByteBuffer /* b0_speed* */ pointer);
}
