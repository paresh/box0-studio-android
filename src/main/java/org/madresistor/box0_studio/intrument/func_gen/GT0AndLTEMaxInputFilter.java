/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.intrument.func_gen;

import org.madresistor.box0_studio.extra.input_filter.ValidatorInputFilter;

/**
 * Greater than 0 and Less than Maximum
 *
 * The class act as a input filter for long values.
 * the value should be greater than 0 and less than the limit.
 *
 * User can define the maximum (limit) value
 *  using {@link #setMaximum(long value)}
 *
 * User can also disable the maximum (limit) check by passing
 *  0 (or negative value) to {@link #setMaximum(long value)}.
 */
public class GT0AndLTEMaxInputFilter extends ValidatorInputFilter
{
	private long m_max = 0;

	/**
	 * Set maximum value
	 * <p>set value to 0 (or negative value) to disable maximum check
	 *
	 * @param value Value
	 */
	public void setMaximum(long value) {
		m_max = value;
	}

	@Override
	protected boolean isValid(String strValue) {
		if (strValue.length() == 0) {
			return true;
		}

		try {
			long value = Long.parseLong(strValue);

			/* atleast greater than 0 */
			if (value <= 0) {
				return false;
			}

			/* if m_max is greater than 0, compare with it */
			if (m_max > 0) {
				if (value > m_max) {
					return false;
				}
			}
		} catch (NumberFormatException nfe) {
			return false;
		}

		return true;
	}
}
