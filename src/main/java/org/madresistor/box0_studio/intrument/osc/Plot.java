/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.intrument.osc;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.WindowManager;

import org.madresistor.box0_studio.Config;
import org.madresistor.box0_studio.extra.SettingsFragment;
import org.madresistor.box0_studio.extra.replot.Cartesian;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import java.nio.ByteBuffer;

public class Plot extends Cartesian {
	private static final String TAG = Plot.class.getName();

	public Plot(Context context) {
		super(context);
	}

	public Plot(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		super.onSurfaceChanged(gl, width, height);
		onSurfaceChanged(getNative());
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		onDrawFrame(getNative());
	}

	private native static void onDrawFrame(ByteBuffer /* (replot_cartesian*) */ arg);
	private native static void onSurfaceChanged(ByteBuffer /* (replot_cartesian*) */ arg);
}
