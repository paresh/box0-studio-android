/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.nav_drawer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {
	private ArrayList<Entry> m_entries;
	LayoutInflater m_inflater;

	public interface Entry {
		public View getView(View convertView, ViewGroup parentView, LayoutInflater inflater);
		public String getTitle();
	}

	public NavDrawerListAdapter(Context context, ArrayList<? extends Entry> entries) {
		m_entries = (ArrayList<Entry>) entries;
		m_inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return m_entries.size();
	}

	@Override
	public Object getItem(int position) {
		return m_entries.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parentView) {
		return m_entries.get(position).getView(convertView, parentView, m_inflater);
	}
}
