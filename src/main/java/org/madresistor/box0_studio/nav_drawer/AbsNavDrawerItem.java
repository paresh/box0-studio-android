/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.box0_studio.nav_drawer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

abstract class AbsNavDrawerItem implements NavDrawerListAdapter.Entry {
	private String m_title;
	private int m_icon;

	private int m_layoutResId;
	private int m_titleResId;
	private int m_iconResId;

	protected AbsNavDrawerItem(int layoutResId, int titleResId, String title, int iconResId, int icon) {
		m_title = title;
		m_icon = icon;

		m_layoutResId = layoutResId;
		m_titleResId = titleResId;
		m_iconResId = iconResId;
	}

	private static class Tag {
		TextView labelView;
		ImageView iconView;
	}

	@Override
	public String getTitle() {
		return m_title;
	}

	@Override
	public View getView(View convertView, ViewGroup parentView, LayoutInflater inflater) {
		Tag tag = null;

		if (convertView == null) {
			tag = new Tag();
			convertView = inflater.inflate(m_layoutResId, parentView, false);
			tag.labelView = (TextView) convertView.findViewById(m_titleResId);
			tag.iconView = (ImageView) convertView.findViewById(m_iconResId);
			convertView.setTag(tag);
		} else {
			tag = (Tag) convertView.getTag();
		}

		tag.labelView.setText(m_title);
		tag.iconView.setImageResource(m_icon);
		return convertView;
	}
}
