/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "private.h"

void throw_exception(JNIEnv *env, const char *cls, const char *msg)
{
	jstring sMsg =(*env)->NewStringUTF(env, msg);
	jclass exClass = (*env)->FindClass(env, cls);
	jmethodID constructor = (*env)->GetMethodID(env, exClass, "<init>", "(Ljava/lang/String;)V");

	jobject exception =(*env)->NewObject(env, exClass, constructor, sMsg);

	(*env)->Throw(env, (jthrowable) exception);

	(*env)->DeleteLocalRef(env, exClass);
}

JavaVM *cachedJVM;
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved)
{
	UNUSED(reserved);

	cachedJVM = jvm;

	return JNI_VERSION_1_6;
}
