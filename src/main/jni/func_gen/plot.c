/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "private.h"
#include "org_madresistor_box0_studio_intrument_func_gen_Plot.h"
#include "replot/cartesian.h"
#include <stdlib.h>
#include <math.h>

static struct {
	lp_cartesian_curve *curve;

	float line_width;

	struct _equation_data {
		float *data;
		size_t count;
		float min, max;
	} x, y;
} plot = {
	.curve = NULL,
	.line_width = 1,
	.x = {NULL, 0, 0, 1},
	.y = {NULL, 0, 0, 1}
};

#define EXTRACT_ARG(env, obj)	\
	((obj != JNI_NULL) ? EXTRACT_POINTER(env, obj, replot_cartesian) : NULL)

/*
 * Class:     org_madresistor_box0_studio_intrument_func_gen_Plot
 * Method:    onDrawFrame
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_func_1gen_Plot_onDrawFrame
  (JNIEnv *env, jclass cls, jobject _arg)
{
	glClear(GL_COLOR_BUFFER_BIT);

	replot_cartesian *arg = EXTRACT_ARG(env, _arg);
	RETURN_IF_NULL(arg);

	lp_cartesian_draw_start(arg->cartesian);

	if (plot.curve != NULL && plot.x.count > 0 && plot.y.count > 0) {
		lp_cartesian_draw_curve(arg->cartesian, plot.curve);
	}

	lp_cartesian_draw_end(arg->cartesian);
}

static void set_data(lp_cartesian_curve *curve,
		lp_cartesian_curve_data_axis axis, struct _equation_data *data)
{
	lp_cartesian_curve_data(curve, axis, 0,
		GL_FLOAT, sizeof(float), data->data, data->count);
}

static void copy_data(JNIEnv *env, jfloatArray src, struct _equation_data *dest)
{
	size_t len = 0;;
	float *data = NULL;

	if (src == NULL) {
		goto done;
	}

	len = (*env)->GetArrayLength(env, src);
	if (!len) {
		goto done;
	}

	/* allocate memory for new data*/
	data = malloc(sizeof(float) * len);
	RETURN_IF_NULL(data);

	/* copy the data to dest buffer */
	(*env)->GetFloatArrayRegion(env, src, 0, len, data);

	done:

	/* free previous array */
	if (dest->data != NULL) {
		free(dest->data);
	}

	dest->count = len;
	dest->data = data;
}

static void find_max_min(struct _equation_data *data)
{
	size_t i;
	float max = -INFINITY, min = +INFINITY, val;

	for (i = 0; i < data->count; i++) {
		val = data->data[i];
		min = fminf(val, min);
		max = fmaxf(val, max);
	}

	RETURN_IF_FAIL(isinf(max) == 0);
	RETURN_IF_FAIL(isinf(min) == 0);

	data->min = min;
	data->max = max;
}

static void show_full(lp_cartesian_axis *axis, struct _equation_data *data)
{
	const float percentage = 10;
	float min = data->min;
	float max = data->max;
	float value = (max - min) * (percentage / 100.0);

	min -= value;
	max += value;
	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

static void update_axis(replot_cartesian *arg)
{
	show_full(arg->axis_bottom, &plot.x);
	show_full(arg->axis_left, &plot.y);
}

static void gen_curve()
{
	if (plot.curve != NULL) {
		return;
	}

	plot.curve = lp_cartesian_curve_gen();
	lp_cartesian_curve_float(plot.curve,
			LP_CARTESIAN_CURVE_LINE_WIDTH, plot.line_width);
}

/*
 * Class:     org_madresistor_box0_studio_intrument_func_gen_Plot
 * Method:    setData
 * Signature: (Ljava/nio/ByteBuffer;[F[F)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_func_1gen_Plot_setData
  (JNIEnv *env, jclass cls, jobject _arg, jfloatArray _x, jfloatArray _y)
{
	UNUSED(cls);

	copy_data(env, _x, &plot.x);
	copy_data(env, _y, &plot.y);

	gen_curve();

	set_data(plot.curve, LP_CARTESIAN_CURVE_X, &plot.x);
	set_data(plot.curve, LP_CARTESIAN_CURVE_Y, &plot.y);

	/* extract max and min */
	if (plot.x.count > 1) {
		plot.x.min = plot.x.data[0];
		plot.x.max = plot.x.data[plot.x.count - 1];
	}

	if (plot.y.count > 1) {
		find_max_min(&plot.y);
	}

	replot_cartesian *arg = EXTRACT_ARG(env, _arg);
	RETURN_IF_NULL(arg);

	update_axis(arg);
}

/*
 * Class:     org_madresistor_box0_studio_intrument_func_gen_Plot
 * Method:    onSurfaceChanged
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_func_1gen_Plot_onSurfaceChanged
  (JNIEnv *env, jclass cls, jobject _arg)
{
	UNUSED(cls);

	replot_cartesian *arg = EXTRACT_ARG(env, _arg);
	RETURN_IF_NULL(arg);

	update_axis(arg);
}

/*
 * Class:     org_madresistor_box0_studio_intrument_func_gen_Plot
 * Method:    onDoubleTap
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_func_1gen_Plot_onDoubleTap
  (JNIEnv *env, jclass cls, jobject _arg)
{
	UNUSED(cls);

	replot_cartesian *arg = EXTRACT_ARG(env, _arg);
	RETURN_IF_NULL(arg);

	update_axis(arg);
}

/*
 * Class:     org_madresistor_box0_studio_intrument_func_gen_Plot
 * Method:    setLineWidth
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_func_1gen_Plot_setLineWidth
  (JNIEnv *env, jclass cls, jfloat line_width)
{
	plot.line_width = line_width;

	RETURN_IF_NULL(plot.curve);
	lp_cartesian_curve_float(plot.curve,
		LP_CARTESIAN_CURVE_LINE_WIDTH, line_width);
}
