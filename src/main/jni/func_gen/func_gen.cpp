/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "org_madresistor_box0_studio_intrument_func_gen_FuncGen.h"
#include "../private.h"
#include <math.h>
#include <muParser.h>
#include <math.h>
#include <stdlib.h>

char* do_equation_to_array(const char *eqation, double freq,
	double min, double max, float *x, float *y, size_t size);

/*
 * Class:     org_madresistor_box0_studio_intrument_func_gen_FuncGen
 * Method:    equation_to_array
 * Signature: (Ljava/lang/String;DDD[F[FI)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_func_1gen_FuncGen_equation_1to_1array
  (JNIEnv *env, jclass cls, jstring _equation, jdouble freq, jdouble min, jdouble max, jfloatArray _x, jfloatArray _y, jint count)
{
	/* extract equation from java string
	 * WARNING: this is a modified utf8, *NOT* an ascii string
	 */
	const char *equation = env->GetStringUTFChars(_equation, NULL);
	if (equation == NULL) {
		return;
	}

	/* get native array from java array */
	jfloat *x = NULL;
	if (_x != NULL) {
		x = env->GetFloatArrayElements(_x, NULL);
	}

	jfloat *y = env->GetFloatArrayElements(_y, NULL);
	if (y == NULL) {
		return;
	}

	/* solve the equation */
	char *problem_stmt = do_equation_to_array(equation, freq, min, max, x, y, count);

	/* free native equation */
	env->ReleaseStringUTFChars(_equation, equation);

	/* free native data */
	if (x != NULL) {
		env->ReleaseFloatArrayElements(_x, x, 0);
	}

	env->ReleaseFloatArrayElements(_y, y, 0);

	/* their was a problem solving the equation? */
	if (problem_stmt != NULL) {
		THROW_RUNTIME_EXCEPTION(env, problem_stmt);
		free(problem_stmt); /* our duty! */
	}
}

char* do_equation_to_array(const char *equation,
	double freq, double min, double max,
	float *save_x, float *save_y, size_t count) {
	mu::Parser parser;

	LOG_DEBUG("assigning equation: %s", equation);
	parser.SetExpr(equation);

	double parser_x, parser_i;

	parser.DefineVar("x", &parser_x);
	parser.DefineVar("i", &parser_i);
	parser.DefineConst("FREQ", freq);
	parser.DefineConst("COUNT", count);

	parser.DefineConst("PI", M_PI);
	parser.DefineConst("EN", M_E);

	LOG_DEBUG("performing calculation (count: %zu)", size);

	/* calculate all values */
	char *problem_stmt = NULL;
	try {
		for (size_t i = 0; i < count; i++) {
			parser_i = i;
			parser_x = i / (count * freq);
			if (save_x != NULL) {
				save_x[i] = parser_x;
			}
			double parser_y = parser.Eval();
			save_y[i] = CLAMP(parser_y, min, max);
		}
	} catch (mu::Parser::exception_type &e) {
		/* assuming message wont exceed 1000 chars */
		problem_stmt = (char *)malloc(1000);
		/* HOPE so problem_stmt is not null now */

		const char *fmt =
			"MuParser (the equation solving library) has returned error while calculating.\n"
			"Message: %s\n"
			"Formula: %s\n"
			"Token: %s\n"
			"Position: %i\n"
			"Errc: %i";
		const char *msg = e.GetMsg().c_str();
		const char *expr = e.GetExpr().c_str();
		const char *token = e.GetToken().c_str();
		int pos = e.GetPos();
		int code =  e.GetCode();
		snprintf(problem_stmt, 1000, fmt, msg, expr, token, pos, code);
	}

	if (problem_stmt == NULL) {
		LOG_DEBUG("done performing calculation");
	} else {
		LOG_DEBUG("done performing calculation with error (%s)", problem_stmt);
	}

	return problem_stmt;
}

/*
 * Class:     org_madresistor_box0_studio_intrument_func_gen_FuncGen
 * Method:    getMaximumSpeed
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_org_madresistor_box0_1studio_intrument_func_1gen_FuncGen_getMaximumSpeed
  (JNIEnv *env, jobject obj, jobject _speed)
{
	UNUSED(obj);
	b0_speed *speed = (b0_speed *) env->GetDirectBufferAddress(_speed);

	uint32_t max = speed->values[0];
	for (size_t i = 0; i < speed->values_len; i++) {
		uint32_t val = speed->values[i];
		max = MAX(max, val);
	}

	return max;
}
