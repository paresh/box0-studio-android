/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "private.h"
#include "replot/cartesian.h"
#include "org_madresistor_box0_studio_intrument_osc_Osc.h"
#include "org_madresistor_box0_studio_intrument_osc_Plot.h"
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>

/**
 * Data is kept in single VBO for all channels.
 * assuming three channels are enabled:
 *
 * ch0.x ch0.y ch1.x ch1.y  ch2.x ch2.y ch0.x ch0.y ch1.x ......
 */

#define WAVEFORM_BUFFER_SECONDS 3

#define EXTRACT_ARG(env, obj)	\
	((obj != JNI_NULL) ? EXTRACT_POINTER(env, obj, replot_cartesian) : NULL)
#define EXTRACT_AIN(env, obj) EXTRACT_POINTER(env, obj, b0_ain)
#define EXTRACT_STREAM_VALUE(env, obj) EXTRACT_POINTER(env, obj, b0_stream_value)

static void* poller_func(void *arg);

static struct {
	lp_vec2f *values;
	size_t total, valid, last_valid;

	/* opengl */
	bool init_vbo;
	GLuint vbo;

	/* only required for circular plot */
	struct { size_t producer, consumer; } last_i;

	/* TODO: multiple curve
	*
	*  producer: (AIN stream):
	*   copy new data to buffer
	*   [hold lock]
	*   update the new last_i.producer
	*   update valid
	*   [release lock]
	*
	*  consumer: (OpenGL)
	*   [hold lock]
	*   store last_i.producer into local variable
	*   [release lock]
	*   copy the specific data to opengl
	*
	* note: the above algorith assume that their is only one consumer and one producer
	*
	* rules infered from above:
	* `valid` can only be written after holding this lock.
	* `last_i.producer` can only be readen/written after holding this lock
	* consumer can only read `last_i.producer` to get calculate size of data need to be copied
	*
	*/
	pthread_mutex_t lock;
} buffer = {
	.values = NULL,
	.total = 0,
	.valid = 0,
	.last_valid = 0,

	.init_vbo = false,
	.vbo = 0
};

/* perform replot | callback to java */
static struct {
	jobject object;
	jmethodID method;
} replot_callback;

/* poller thread */
static struct {
	pthread_t thread;
	volatile bool interruption_requested;
	b0_ain *ain;
	size_t count;
} poller = {
	.interruption_requested = false,
	.ain = NULL,
	.count = 0
};

/* curves common data */
static struct {
	unsigned total_curves;
	lp_cartesian_curve **curves;
} plot = {
	.total_curves = 0,
	.curves = NULL,
};

static void curves_free(void)
{
	size_t i;

	LOG_DEBUG("freeing curves");

	/* free individual curves */
	for (i = 0; i < plot.total_curves; i++) {
		lp_cartesian_curve_del(plot.curves[i]);
	}

	free(plot.curves);
	plot.curves = NULL;
	plot.total_curves = 0;
}

static void curves_alloc(size_t count, lp_color4f *colors, float line_width)
{
	size_t i;
	lp_cartesian_curve *crv;
	plot.total_curves = count;

	LOG_DEBUG("allocating %zu curves", count);
	plot.curves = calloc(count, sizeof(*plot.curves));
	RETURN_IF_NULL(plot.curves);

	for (i = 0; i < count; i++) {
		crv = lp_cartesian_curve_gen();
		if (crv == NULL) {
			LOG_WARN("lp_cartesian_curve_gen return null for curve %zu", i);
			return;
		}

		plot.curves[i] = crv;
		lp_cartesian_curve_4float(crv, LP_CARTESIAN_CURVE_LINE_COLOR,
			colors[i].r, colors[i].g, colors[i].b, colors[i].a);
		lp_cartesian_curve_bool(crv, LP_CARTESIAN_CURVE_LINE_SHOW, true);
		lp_cartesian_curve_float(crv, LP_CARTESIAN_CURVE_LINE_WIDTH, line_width);
	}
}

static void plot_replot_callback_free(JNIEnv *env)
{
	LOG_DEBUG("freeing object AIN (and method) for callback");
	if (replot_callback.object != 0) {
		(*env)->DeleteGlobalRef(env, replot_callback.object);
	}

	replot_callback.object = replot_callback.method = 0;
}

static void plot_replot_callback_alloc(JNIEnv *env, jobject obj)
{
	LOG_DEBUG("cachaing object AIN (and method) for callback");
	/* cache java object for callback */
	replot_callback.object = (jobject) (*env)->NewGlobalRef(env, obj);
	jclass _cls = (*env)->GetObjectClass(env, obj);
	replot_callback.method = (*env)->GetMethodID(env, _cls, "replot", "()V");
}

static void buffer_free(void)
{
	LOG_DEBUG("freeing curve common buffer");
	buffer.total = 0;
	buffer.valid = 0;
	buffer.last_i.producer = buffer.last_i.consumer = 0;
	buffer.last_valid = 0;
	replot_callback.method = replot_callback.object = 0;
	if (buffer.values != NULL) {
		/* free previous memory */
		free(buffer.values);
		buffer.values = NULL;
	}
}

static void buffer_alloc(uint32_t speed, size_t ch_seq_len)
{
	size_t samples = ((size_t)ceil(WAVEFORM_BUFFER_SECONDS / (double)ch_seq_len)) * ch_seq_len * speed;
	size_t byte_len = sizeof(lp_vec2f) * samples;
	LOG_DEBUG(
		"allocating buffer.total: %zu bytes "
		"[speed: %" PRIu32 "] "
		"[ch_seq_len: %zu]",
		byte_len, speed, ch_seq_len);

	lp_vec2f *ptr = (lp_vec2f *) malloc(byte_len);
	if (ptr == NULL) {
		LOG_WARN("unable to allocate memory for buffer.values");
		return;
	}

	/* filling x value (time in seconds) for graph */
	size_t i;
	for (i = 0; i < samples; i++) {
		ptr[i].x = i / (float) speed;
	}

	buffer.values = ptr;
	buffer.total = samples;
	buffer.init_vbo = true;
}

static size_t extract_ch_sequence(JNIEnv *env, jshortArray _ch_seq, uint8_t *ch_seq)
{
	size_t ch_seq_len = (*env)->GetArrayLength(env, _ch_seq);
	if (!ch_seq_len) {
		LOG_WARN("channel sequence length is NULL");
		return 0;
	}

	jshort *ch_seq_arr = (*env)->GetShortArrayElements(env, _ch_seq, NULL);
	if (ch_seq_arr == NULL) {
		LOG_WARN("channel sequence array NULL");
		return 0;
	}

	size_t i;
	for (i = 0; i < ch_seq_len; i++) {
		ch_seq[i] = ch_seq_arr[i];
	}
	(*env)->ReleaseShortArrayElements(env, _ch_seq, ch_seq_arr, JNI_ABORT);

	char msg[50];
	size_t offset = 0;
	for (i = 0; i < ch_seq_len; i++) {
		size_t max_len = sizeof(msg) - offset;
		const char *fmt = (i > 0) ? ", %"PRIu8 : "%"PRIu8;
		int size = snprintf(&msg[offset], max_len, fmt, ch_seq[i]);
		if (!(size >= 0 && size <= max_len)) {
			break;
		}

		offset += size;
	}

	LOG_DEBUG("channel sequence is %s", msg);

	return ch_seq_len;
}


static void convert_colors(JNIEnv *env, jintArray _colors, lp_color4f *colors, size_t len)
{
	size_t i, actual_len;

	/* verify length is ok */
	actual_len = (*env)->GetArrayLength(env, _colors);
	if (actual_len < len) {
		LOG_WARN("colors array is smaller in length than expected");
		return;
	}

	/* get the color values */
	jint *colors_int = (*env)->GetIntArrayElements(env, _colors, NULL);
	if (colors == NULL) {
		LOG_WARN("colors array is NULL");
		return;
	}

	/* convert the color values */
	for (i = 0; i < len; i++) {
		uint32_t color = (uint32_t) colors_int[i];
		colors[i].b = ((color >> 0) & 0xFF) / 255.0;
		colors[i].g = ((color >> 8) & 0xFF) / 255.0;
		colors[i].r = ((color >> 16) & 0xFF) / 255.0;
		colors[i].a = ((color >> 24) & 0xFF) / 255.0;
	}

	(*env)->ReleaseIntArrayElements(env, _colors, colors_int, JNI_ABORT);
}

/*
 * Class:     org_madresistor_box0_studio_intrument_osc_Osc
 * Method:    box0AinStreamStart
 * Signature: (Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;[SZ[IF)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_osc_Osc_box0AinStreamStart
  (JNIEnv *env, jobject obj, jobject _mod, jobject _stream_value,
  jshortArray _ch_seq, jboolean flowPlot, jintArray _colors, jfloat line_width)
{
	uint8_t ch_seq[256];
	lp_color4f colors[256];

	LOG_DEBUG("box0AinStreamStart");

	/* Convert jshortArray to uint8_t[] */
	size_t ch_seq_len = extract_ch_sequence(env, _ch_seq, ch_seq);

	b0_ain *mod = EXTRACT_AIN(env, _mod);
	b0_stream_value *stream_value = EXTRACT_STREAM_VALUE(env, _stream_value);

	LOG_DEBUG("Streaming Speed: %"PRIu32", Bitsize: %"PRIu8,
		stream_value->speed, stream_value->bitsize);

	/* init lock */
	if (pthread_mutex_init(&buffer.lock, NULL) < 0) {
		LOG_WARN("unable to init lock");
		return;
	}

	curves_free();
	buffer_free();
	plot_replot_callback_free(env);

	convert_colors(env, _colors, colors, ch_seq_len);

	buffer_alloc(stream_value->speed, ch_seq_len);
	curves_alloc(ch_seq_len, colors, line_width);
	plot_replot_callback_alloc(env, obj);

	b0_result_code r = B0_OK;

	if (B0_ERR_RC(r = b0_ain_stream_prepare(mod, stream_value)) ||
		B0_ERR_RC(r = b0_chan_seq_set(mod->chan_seq, ch_seq, ch_seq_len)) ||
		B0_ERR_RC(r = b0_ain_stream_start(mod))) {

		curves_free();
		buffer_free();
		plot_replot_callback_free(env);

		ThrowBox0Exception(r);
		return;
	}

	/* calculate the number of sample to grab in poll.
	 *  also, make sure that count is a multiple of chan_seq_len
	 *  (in order to make the copy algorithm simple) */
	size_t count = stream_value->speed / 10;
	count -= count % ch_seq_len;

	/* poller parameters */
	poller.count = count;
	poller.ain = mod;
	poller.interruption_requested = false;

	/* start poller thread */
	if (pthread_create(&poller.thread, NULL, poller_func, NULL) < 0) {
		THROW_RUNTIME_EXCEPTION(env, "unable to start poller");
	}
}

/*
 * send a RenderRequest to Java
 */
static void* send_replot_request_to_java(void *ign)
{
	/* generate enviroment variable */
	JNIEnv *env;
	int getEnvStat = (*cachedJVM)->GetEnv(cachedJVM, (void **)&env, JNI_VERSION_1_6);
	if (getEnvStat == JNI_EDETACHED) {
		if ((*cachedJVM)->AttachCurrentThread(cachedJVM, &env, NULL) != 0) {
			LOG_DEBUG("failed to attach");
			return NULL;
		}
	} else if (getEnvStat == JNI_OK) {
		// nothing to do
	} else if (getEnvStat == JNI_EVERSION) {
		LOG_DEBUG("getEnvStat version not supported");
	}

	/* call java method */
	(*env)->CallVoidMethod(env, replot_callback.object, replot_callback.method);


	/* cleanup */
	(*cachedJVM)->DetachCurrentThread(cachedJVM);
	return NULL;
}

static void circular_copy(float *data, size_t len)
{
	LOG_DEBUG("copying data");
	size_t i = 0;
	size_t j = buffer.last_i.producer;
	while (i < len) {
		buffer.values[j++].y = data[i++];

		/* circular buffer */
		if (!(j < buffer.total)) {
			j = 0;
		}
	}

	if (pthread_mutex_lock(&buffer.lock) < 0) {
		LOG_WARN("unable to hold crv->lock");
		return;
	}
	LOG_DEBUG("holding crv->lock");

	LOG_DEBUG("updaing length");
	buffer.last_i.producer = j;

	buffer.valid += len;
	if (buffer.valid > buffer.total) {
		buffer.valid = buffer.total;
	}

	if (pthread_mutex_unlock(&buffer.lock) < 0) {
		LOG_WARN("unable to release lock");
		return;
	}
	LOG_DEBUG("released crv->lock");
}

static void* poller_func(void *arg)
{
	UNUSED(arg);

	/* allocate memory for holding data */
	size_t count = poller.count;
	float *values = malloc(sizeof(float) * count);
	if (values == NULL) {
		LOG_WARN("malloc returned NULL");
		return NULL;
	}

	/* poll data till not requested to exit */
	b0_ain *ain = poller.ain;
	while (!poller.interruption_requested) {
		/* poll */
		if (b0_ain_stream_read_float(ain, values, count, NULL) < 0) {
			LOG_WARN("b0_ain_stream_read_float() failed");
			break;
		}

		/* copy data */
		circular_copy(values, count);

		/* tell java to replot() in new thread */
		pthread_t _ign;
		if (pthread_create(&_ign, NULL, send_replot_request_to_java, NULL) < 0) {
			LOG_WARN("unable to start new thread for `send_replot_request_to_java`");
			break;
		}
	}

	/* free up allocate resources */
	free(values);

	return NULL;
}

/*
 * Class:     org_madresistor_box0_studio_intrument_osc_Osc
 * Method:    box0AinStreamStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_osc_Osc_box0AinStreamStop
  (JNIEnv *env, jclass cls, jobject _mod)
{
	UNUSED(cls);
	LOG_DEBUG("box0AinStreamStop");

	b0_ain *mod = EXTRACT_AIN(env, _mod);

	/* stop the poller */
	poller.interruption_requested = true;
	if (pthread_join(poller.thread, NULL)) {
		THROW_RUNTIME_EXCEPTION(env, "unable to join thread");
		return;
	}

	/* stop device */
	b0_result_code r = b0_ain_stream_stop(mod);

	plot_replot_callback_free(env);

	if (B0_ERR_RC(r)) {
		ThrowBox0Exception(r);
	}
}

static void init_vbo()
{
	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, buffer.vbo));

	size_t byte_len = buffer.total * sizeof(lp_vec2f);
	LOG_DEBUG("allocating buffer (%zu bytes) in VBO buffer.vbo", byte_len);
	LOG_GL_ERROR(glBufferData(GL_ARRAY_BUFFER, byte_len, NULL, GL_STREAM_DRAW));

	/* assure we have curve */
	if (!plot.total_curves) {
		return;
	}

	/* ontime init of X values */
	size_t i;
	GLsizei stride = plot.total_curves * sizeof(lp_vec2f);
	GLsizei total_sample_per_ch = buffer.total / plot.total_curves;
	for (i = 0; i < plot.total_curves; i++) {
		GLvoid *offset = NULL + (i * sizeof(lp_vec2f)) + offsetof(lp_vec2f, x);

		lp_cartesian_curve_data(plot.curves[i], LP_CARTESIAN_CURVE_X,
			buffer.vbo, GL_FLOAT, stride, offset, total_sample_per_ch);
	}
}

static void copy_previous_data_to_vbo()
{
	/* check if we have values to copy */
	if (buffer.values == NULL) {
		LOG_WARN("buffer.values is NULL, cannot copy previous data");
		return;
	}

	/* hold lock */
	if (pthread_mutex_lock(&buffer.lock) < 0) {
		LOG_WARN("unable to hold lock");
		return;
	}

	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, buffer.vbo));

	/* copy data */
	size_t byte_len = sizeof(lp_vec2f) * buffer.last_valid;
	LOG_DEBUG("copying old value (%zu bytes) to VBO buffer.vbo", byte_len);
	LOG_GL_ERROR(glBufferSubData(GL_ARRAY_BUFFER, 0, byte_len, buffer.values));

	/* release lock */
	if (pthread_mutex_unlock(&buffer.lock) < 0) {
		LOG_WARN("unable to unlock");
	}
}

void copy_data_to_vbo()
{
	size_t i, last_i_producer, valid;

	/* check if we have values for vbo */
	if (buffer.values == NULL) {
		LOG_WARN("buffer.values is NULL, cannot initalize");
		return;
	}

	/* vbo to copy new data to. */
	LOG_DEBUG_INT(buffer.vbo);
	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, buffer.vbo));

	/* try to hold lock so that new data can be checked.
	 * if not, just draw old values or else it will cause flickering
	 */
	if (pthread_mutex_trylock(&buffer.lock) < 0) {
		LOG_WARN("unable to hold lock (data being modified)");
		return;
	}

	LOG_DEBUG("holding crv->lock");

	/* cache value in local variables */
	last_i_producer = buffer.last_i.producer;
	valid = buffer.valid;

	if (pthread_mutex_unlock(&buffer.lock) < 0) {
		LOG_WARN("unable to release lock");
	}

	LOG_DEBUG("crv->lock released");
	LOG_DEBUG("last_i_producer = %zu, valid = %zu", last_i_producer, valid);

	size_t offset, size;
	void *ptr = buffer.values;

	if (last_i_producer > buffer.last_i.consumer) {
		/* data is in one contigeous block */
		offset = sizeof(lp_vec2f) * buffer.last_i.consumer;
		size = sizeof(lp_vec2f) * (last_i_producer - buffer.last_i.consumer);
		LOG_GL_ERROR(glBufferSubData(GL_ARRAY_BUFFER, offset, size, ptr + offset));
	} else if (last_i_producer < buffer.last_i.consumer) {
		/* data is in two parts. because to implement
		 * a virual circular buffer, this case arise
		 * when some data is at the end and some at head */

		/* data at end */
		offset = sizeof(lp_vec2f) * buffer.last_i.consumer;
		size = sizeof(lp_vec2f) * (buffer.total - buffer.last_i.consumer);
		LOG_GL_ERROR(glBufferSubData(GL_ARRAY_BUFFER, offset, size, ptr + offset));

		/* data at head */
		offset = 0;
		size = sizeof(lp_vec2f) * last_i_producer;
		glBufferSubData(GL_ARRAY_BUFFER, offset, size, ptr + offset);
	} else {
		LOG_DEBUG("data not modified, nothing copied");
		return;
	}

	/* update only if new data was copied to opengl buffer */
	buffer.last_i.consumer = last_i_producer;
	buffer.last_valid = valid;

	/* update curves (Y axis) [x is already initalized in init_vbo()] */
	GLsizei stride = plot.total_curves * sizeof(lp_vec2f);
	for (i = 0; i < plot.total_curves; i++) {
		GLsizei count = (buffer.last_valid - i) / plot.total_curves;
		GLvoid *offset = NULL + (i * sizeof(lp_vec2f)) + offsetof(lp_vec2f, y);

		lp_cartesian_curve_data(plot.curves[i], LP_CARTESIAN_CURVE_Y,
			buffer.vbo, GL_FLOAT, stride, offset, count);
	}
}

/*
 * Class:     org_madresistor_box0_studio_intrument_osc_Plot
 * Method:    onDrawFrame
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_osc_Plot_onDrawFrame
  (JNIEnv *env, jclass cls, jobject obj)
{
	glClear(GL_COLOR_BUFFER_BIT);

	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	RETURN_IF_NULL(arg);

	/* At this point vbo might be invalid.
	 *  we need to allocate the vbo in order to continue further.
	 */
	if (buffer.init_vbo) {
		init_vbo();
		buffer.init_vbo = false;
	}

	copy_data_to_vbo();

	lp_cartesian_draw_start(arg->cartesian);

	size_t i;
	for (i = 0; i < plot.total_curves; i++) {
		lp_cartesian_draw_curve(arg->cartesian, plot.curves[i]);
	}

	lp_cartesian_draw_end(arg->cartesian);
}

/*
 * Class:     org_madresistor_box0_studio_intrument_osc_Plot
 * Method:    onSurfaceChanged
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_box0_1studio_intrument_osc_Plot_onSurfaceChanged
  (JNIEnv *env, jclass cls, jobject obj)
{
	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	RETURN_IF_NULL(arg);

	/* At this point, the osc might be running.
	 * but since the surface has changed, vbo is invalided.
	 * we need to reallocate vbo
	 */
	LOG_DEBUG("generating VBO buffer.vbo");
	LOG_GL_ERROR(glGenBuffers(1, &buffer.vbo));

	init_vbo();

	/* vbo is initalized */
	buffer.init_vbo = false;

	/* copy any previous value to vbo
	 * occur when gl context is recreated any their is still data to be shown
	 * ex: stopped and rotated
	 */
	copy_previous_data_to_vbo();

	lp_cartesian_axis_pointer(arg->axis_left, LP_CARTESIAN_AXIS_VALUE_APPEND, "V");
	lp_cartesian_axis_pointer(arg->axis_bottom, LP_CARTESIAN_AXIS_VALUE_APPEND, "s");
}
